#!/bin/bash
#=
if [ -z "$(which realpath)" ]; then
macos_realpath=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
PROJECT_DIR=$(dirname $macos_realpath)
else
PROJECT_DIR=$(dirname $(dirname $(realpath "${BASH_SOURCE[0]}")))
fi
FLAGS=
if [ "$1" = "--sysimage" -o "$1" = "-J" ]; then FLAGS="--sysimage $2 "; shift 2; fi
if [ "${1:0:2}" = "-J" ]; then FLAGS="$1 "; shift; fi
HELP=
for i in "$@"; do if [ "$i" = "-h" -o "$i" = "--help" ]; then HELP=1; break; fi; done
if [ -z $HELP ]; then FLAGS="$FLAGS -iq "; fi
if [ -z "$JULIA" ]; then JULIA=julia; fi
    exec $JULIA --project="$PROJECT_DIR" --color=yes --startup-file=no $FLAGS\
    "${BASH_SOURCE[0]}" "$@"
=#

projectdir = dirname(Base.active_project())

include(joinpath(projectdir, "src/cli_open.jl")) # defines `main`

using .GUI

main();
