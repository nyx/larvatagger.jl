#!/bin/bash

for _ in $(seq $#); do
  case $1 in
    open|import|merge|train|predict|-V|--version|--more-help|reverse-mapping|confusion)
      cmd=$1
      shift
      break
      ;;
    build|-h|--help|--update)
      cmd=$1
      shift
      without_rm=1
      break
      ;;
    --no-rm)
      without_rm=1
      shift
      ;;
    --rm)
      # default since v0.16
      shift
      ;;
    --with-cache)
      cache=1
      shift
      ;;
    *)
      if [ "$1" = "--no-cache" ]; then
        # --no-cache is default for build since 0.16.1
        no_cache=1
      elif [ "$1" = "--target" ]; then
        DOCKER_ARGS="${DOCKER_ARGS}$1 "
        shift
        target=1
      fi
      # note: if DOCKER_ARGS is externally defined, it must end with an explicit space
      DOCKER_ARGS="${DOCKER_ARGS}$1 "
      shift
  esac
done

if [ -z "$without_rm" ]; then
  DOCKER_ARGS="${DOCKER_ARGS}--rm "
fi

if [ -z "$docker" ]; then
  docker=docker
  if command -v podman &> /dev/null; then
    docker=podman
    if [ "$cmd" != "build" ]; then
      DOCKER_ARGS="${DOCKER_ARGS}--security-opt label=disable "
    fi
  fi
else
  echo "Using environment variable: docker= $docker"
fi

if [ -z "$LARVATAGGER_IMAGE" ]; then
if [ "$cmd" = "build" -o -n "$($docker images | grep '^larvatagger ')" ]; then
LARVATAGGER_IMAGE=larvatagger
else
LARVATAGGER_IMAGE=flaur/larvatagger
fi
else
echo "Using environment variable: LARVATAGGER_IMAGE= $LARVATAGGER_IMAGE"
fi

HOST_UID=$(id -u $USER)
HOST_GID=$(id -g $USER)
RUN_ARGS="-e HOST_UID=$HOST_UID -e HOST_GID=$HOST_GID"

case "$cmd" in

	build)

if ! [ -f recipes/Dockerfile ]; then
echo "the build command can only be run from the project root directory"
exit 1
fi
TARGET=base
while ! [ -z "$1" ]; do
if [ "$1" = "--dev" -o "$1" = "--stable" ]; then
BUILD=$1; shift
elif [ "$1" = "--get-backend" -o "$1" = "--with-default-backend" ]; then
DOCKER_ARGS="--build-arg BACKEND=MaggotUBA/dev $DOCKER_ARGS"; shift
TARGET=backend
elif [ "$1" = "--with-backend" ]; then
DOCKER_ARGS="--build-arg BACKEND=$2 $DOCKER_ARGS"; shift 2
TARGET=backend
elif [ "${1:0:15}" = "--with-backend=" ]; then
DOCKER_ARGS="--build-arg BACKEND=${1:15} $DOCKER_ARGS"; shift
TARGET=backend
elif [ "$1" = "--no-cache" ]; then
echo "Deprecation warning: argument $1 should now be passed before build"
shift
else
echo "argument not supported: $1"; shift
exit 1
fi
done
if [ -z "$no_cache" ] && [ -z "$cache" ]; then
  DOCKER_ARGS="--no-cache $DOCKER_ARGS"
fi
if [ -z "$target" ]; then
DOCKER_ARGS="--target $TARGET $DOCKER_ARGS"
fi
if [ -z "$DOCKERFILE" ]; then
  DOCKERFILE=recipes/Dockerfile
else
  echo "Using environment variable DOCKERFILE= $DOCKERFILE"
fi
DOCKER_ARGS="-f \"$DOCKERFILE\" $DOCKER_ARGS"
if [ "$BUILD" == "--dev" ]; then
if ! [[ "$LARVATAGGER_IMAGE" == *:* ]]; then LARVATAGGER_IMAGE="${LARVATAGGER_IMAGE}:dev"; fi
PROJECT_ROOT=$(basename $(pwd))
cd ..
DOCKER_BUILDKIT=1 $docker build -t "$LARVATAGGER_IMAGE" -f "$PROJECT_ROOT/recipes/Dockerfile.local" ${DOCKER_ARGS}.
elif [ "$BUILD" == "--stable" ]; then
if ! [[ "$LARVATAGGER_IMAGE" == *:* ]]; then LARVATAGGER_IMAGE="${LARVATAGGER_IMAGE}:stable"; fi
$docker build -t "$LARVATAGGER_IMAGE" ${DOCKER_ARGS}.
else
if ! [[ "$LARVATAGGER_IMAGE" == *:* ]]; then LARVATAGGER_IMAGE="${LARVATAGGER_IMAGE}:latest"; fi
if [ -z "$LARVATAGGER_BRANCH" ]; then
  if [ -z "$LARVATAGGER_DEFAULT_BRANCH" ]; then
    LARVATAGGER_BRANCH=dev;
  else
    echo "Deprecation notice: LARVATAGGER_DEFAULT_BRANCH has been renamed LARVATAGGER_BRANCH"
    LARVATAGGER_BRANCH=$LARVATAGGER_DEFAULT_BRANCH
  fi
else
  echo "Using environment variable: LARVATAGGER_BRANCH= $LARVATAGGER_BRANCH"
fi
if [ -z "$TAGGINGBACKENDS_BRANCH" ]; then
  TAGGINGBACKENDS_BRANCH=$LARVATAGGER_BRANCH
else
  echo "Using environment variable: TAGGINGBACKENDS_BRANCH= $TAGGINGBACKENDS_BRANCH"
fi
DOCKER_ARGS="--build-arg TAGGINGBACKENDS_BRANCH=$TAGGINGBACKENDS_BRANCH $DOCKER_ARGS"
DOCKER_BUILD="$docker build -t "$LARVATAGGER_IMAGE" ${DOCKER_ARGS}--build-arg BRANCH=$LARVATAGGER_BRANCH ."
echo $DOCKER_BUILD
eval $DOCKER_BUILD
fi
;;

	open)

if [ -z "$LARVATAGGER_PORT" -o "$LARVATAGGER_PORT" == "9284" ]; then
DOCKER_ARGS="-p 9284:9284 $DOCKER_ARGS"
TAGGER_ARGS=
else
echo "Using environment variable: LARVATAGGER_PORT= $LARVATAGGER_PORT"
DOCKER_ARGS="-p $LARVATAGGER_PORT:$LARVATAGGER_PORT $DOCKER_ARGS"
TAGGER_ARGS="--port=$LARVATAGGER_PORT"
fi

parentdir=$(cd "$(dirname "$1")"; pwd -P)
file=$(basename "$1"); shift

RUN_ARGS="$RUN_ARGS -v \"$parentdir\":/data"

# undocumented feature
backend=MaggotUBA
while [ -n "$1" -a "$1" = "--external-instance" ]; do
instance=$2; shift 2
if ! command -v realpath &>/dev/null; then
echo "realpath: command not found"
echo "on macOS: brew install coreutils"
exit 1
fi
RUN_ARGS="$RUN_ARGS --mount type=bind,src=\"$(realpath $instance)\",dst=/app/$backend/models/$(basename $instance)"
done

DOCKER_RUN="exec $docker run $RUN_ARGS -i ${DOCKER_ARGS}\"$LARVATAGGER_IMAGE\" open \"/data/$file\" $TAGGER_ARGS $@"
echo $DOCKER_RUN
eval $DOCKER_RUN
;;

	import | merge)

parentdir=$(cd "$(dirname "$1")"; pwd -P)
file=$(basename "$1"); shift

RUN_ARGS="$RUN_ARGS -v \"$parentdir\":/data"

eval "$docker run $DOCKER_ARGS$RUN_ARGS \"$LARVATAGGER_IMAGE\" $cmd \"/data/$file\" $@"
;;

	train)

if [ "$(basename "$(dirname "$1")")" = "**" ]; then
parentdir=$(cd "$(dirname "$(dirname "$1")")"; pwd -P)
data_repository=$(basename "$(dirname "$1")")/$(basename "$1")
else
parentdir=$(cd "$(dirname "$1")"; pwd -P)
data_repository=$(basename "$1")
fi
shift

RUN_ARGS="$RUN_ARGS -v \"$parentdir\":/data"

tagger=$1; shift

external_models_dir=models
path=$(dirname $tagger)
if [ "$path" != "." ]; then
external_models_dir=$path
tagger=$(basename $tagger)
fi

backend=MaggotUBA
while [ -n "$1" ]; do
if [ "$1" = "--backend" ]; then
backend=$2; shift 2
elif [ "${1:0:10}" = "--backend=" ]; then
backend="${1:10}"; shift
elif [ "$1" = "--gpus" -o "$1" == "--cpus" -o "$1" = "-m" -o "$1" = "--memory" ]; then
echo "Deprecation warning: argument $1 should now be passed before train"
RUN_ARGS="$RUN_ARGS $1 $2"; shift 2
elif [ "$1" = "--rm" -o "${1:0:7}" = "--gpus=" -o "${1:0:7}" = "--cpus=" -o "${1:0:9}" = "--memory=" ]; then
echo "Deprecation warning: arguments --rm|--cpus|--gpus|--memory should now be passed before train"
RUN_ARGS="$RUN_ARGS $1"; shift
else
break
fi
done

if [ -n "$external_models_dir" ]; then
mkdir -p "$external_models_dir"
src=$(cd "$external_models_dir"; pwd -P)
RUN_ARGS="$RUN_ARGS --mount type=bind,src=\"$src\",dst=/app/$backend/models"
fi
RUN_ARGS="$RUN_ARGS --entrypoint=/app/scripts/larvatagger-toolkit.jl"

DOCKER_RUN="$docker run $DOCKER_ARGS$RUN_ARGS \"$LARVATAGGER_IMAGE\" train \"/app/$backend\" \"/data/$data_repository\" \"$tagger\" $@"
echo $DOCKER_RUN
eval $DOCKER_RUN
;;

	predict)

if [ "$(basename "$(dirname "$1")")" = "**" ]; then
parentdir=$(cd "$(dirname "$(dirname "$1")")"; pwd -P)
data_file=$(basename "$(dirname "$1")")/$(basename "$1")
else
parentdir=$(cd "$(dirname "$1")"; pwd -P)
data_file=$(basename "$1")
fi
shift

RUN_ARGS="$RUN_ARGS -v \"$parentdir\":/data"

backend=MaggotUBA
tagger="20230311"
while [ -n "$1" ]; do
if [ "${1:0:16}" = "--model-instance" ]; then
if [ "$1" = "--model-instance" ]; then
tagger=$2; shift 2
elif [ "${1:0:17}" = "--model-instance=" ]; then
tagger="${1:17}"; shift
else
echo "argument not supported: $1"
exit 1
fi
if [ -d "$tagger" ]; then
tagger_path=$tagger
tagger=$(basename $tagger_path)
fi
elif [ "$1" = "--backend" ]; then
backend=$2; shift 2
elif [ "${1:0:10}" = "--backend=" ]; then
backend="${1:10}"; shift
elif [ "$1" = "--gpus" -o "$1" == "--cpus" -o "$1" = "-m" -o "$1" = "--memory" ]; then
echo "Deprecation warning: argument $1 should now be passed before predict"
RUN_ARGS="$RUN_ARGS $1 $2"; shift 2
elif [ "$1" = "--rm" -o "${1:0:7}" = "--gpus=" -o "${1:0:7}" = "--cpus=" -o "${1:0:9}" = "--memory=" ]; then
echo "Deprecation warning: arguments --rm|--cpus|--gpus|--memory should now be passed before predict"
RUN_ARGS="$RUN_ARGS $1"; shift
else
break
fi
done

if [ -n "$tagger_path" ]; then
if ! command -v realpath &>/dev/null; then
echo "realpath: command not found"
echo "on macOS: brew install coreutils"
exit 1
fi
RUN_ARGS="$RUN_ARGS --mount type=bind,src=\"$(realpath $tagger_path)\",dst=/app/$backend/models/$tagger"
fi

DOCKER_RUN="$docker run $DOCKER_ARGS$RUN_ARGS \"$LARVATAGGER_IMAGE\" predict \"/app/$backend\" \"$tagger\" \"/data/$data_file\" $@"
echo $DOCKER_RUN
eval $DOCKER_RUN
;;

  reverse-mapping)

if [ "$(basename "$(dirname "$1")")" = "**" ]; then
parentdir=$(cd "$(dirname "$(dirname "$1")")"; pwd -P)
output_labels=$(basename "$(dirname "$1")")/$(basename "$1")
else
parentdir=$(cd "$(dirname "$1")"; pwd -P)
output_labels=$(basename "$1")
fi
shift
unmapped_labels=$(basename "$1")
shift
edited_labels=$(basename "$1")
shift

RUN_ARGS="$RUN_ARGS -v \"$parentdir\":/data"

backend=MaggotUBA
tagger="20230311"
DOCKER_RUN="$docker run $DOCKER_ARGS$RUN_ARGS -it --entrypoint julia \"$LARVATAGGER_IMAGE\" \"/app/$backend/scripts/revert_label_mapping.jl\"  \"/data/$output_labels\" \"/data/$unmapped_labels\" \"/data/$edited_labels\" \"/app/$backend/models/$tagger/clf_config.json\""
echo $DOCKER_RUN
eval $DOCKER_RUN
;;

  confusion)

parentdir=$(cd "$1"; pwd -P)
shift

RUN_ARGS="$RUN_ARGS -v \"$parentdir\":/data"

# wherever taggingbackends (python) is installed
backend=MaggotUBA

DOCKER_RUN="$docker run $DOCKER_ARGS$RUN_ARGS -it -w /app/$backend --entrypoint='[\"poetry\", \"run\", \"python\"]' \"$LARVATAGGER_IMAGE\" /bin/confusion.py"
echo "The confusion.py script is shipped only in images built with argument --target confusion"
echo $DOCKER_RUN
eval $DOCKER_RUN
;;

	--more-help)

echo 'For most switches, arguments are passed to larvatagger.jl.'
echo 'larvatagger.jl extra arguments must come after larvatagger.sh arguments.'
echo 'Printing larvatagger.jl --help...'
echo

$docker run ${DOCKER_ARGS}"$LARVATAGGER_IMAGE" --help
;;

	-V|--version)

$docker run ${DOCKER_ARGS}"$LARVATAGGER_IMAGE" --version
;;

	--update)

if [ -n "$($docker images | grep '^larvatagger         latest ')" ]; then
$docker rmi ${DOCKER_ARGS}-f larvatagger:latest
fi
$docker pull ${DOCKER_ARGS}flaur/larvatagger:latest
;;

	*)

cat << EOT
Wrapper script to operate larvatagger.jl in a Docker image.

Usage: $0 build [--stable] [--with-default-backend] [--with-backend <backend>]
       $0 open <filepath> [...]
       $0 import <filepath> [<outputfilename>] [...]
       $0 train <datarepository> <taggername> [--backend <name>] [...]
       $0 predict <datafile> [--backend <name>] [--model-instance <taggername>] [...]
       $0 confusion <datarepository>
       $0 merge <filepath> [<outputfilename>] [...]
       $0 reverse-mapping <filepath> <filename> <outputfilename>
       $0 --more-help
       $0 --version
       $0 --update

Note the arguments are very similar to those of larvatagger.jl, except with the train
command. Indeed, the backend is MaggotUBA-adapter per default, if included in the image.

The build, confusion and reverse-mapping commands are specific to the present script and
do not interface with larvatagger.jl.

The confusion command crawls the data repository (first argument) in search for
groundtruth.label and predicted.label files, and generates a confusion.csv file wherever
both label files are found.

The reverse-mapping command takes two sibling label files, the first one with mapped labels,
the second one with unmapped labels. It generates a third label file with demapped labels
from the first file. This is useful when the first file diverges from the second one by some
manual editions, on top of label mapping.

See --more-help for more information about additional arguments for the other commands from
larvatagger.jl.
EOT
;;

esac
