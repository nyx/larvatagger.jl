<#
.SYNOPSIS
Wrapper for LarvaTagger.jl Docker image

.DESCRIPTION
Usage: larvatagger.ps1 open <filepath>
       larvatagger.ps1 import <filepath> [<outputfilename>]
       larvatagger.ps1 train <datarepository> <taggername>
       larvatagger.ps1 predict <datafile> <taggername>
       larvatagger.ps1 merge <filepath> [<outputfilename>]
       larvatagger.ps1 --more-help
       larvatagger.ps1 --version
       larvatagger.ps1 --update

Argument <taggername> can also be a path, for external storage.
Custom taggers cannot be persistently stored inside the image.
If <taggername> does not include at least one file separator, `train` will save
the files for tagger <taggername> in a *./models* directory it will create if
the directory does not already exist.

Conversely, optional argument <outputfilename> should NOT be a path.
The *label* file it refers to will be created as a sibling of <filepath>, i.e.
in the same parent directory.

As argument <datarepository>, `train` admits either a directory (the
subdirectories are recursively walked), a single tracking data file, or a *.txt*
file that lists tracking data files (one per line).

#>

$LARVATAGGER_IMAGE = 'flaur/larvatagger'
$TAGGING_BACKEND = 'MaggotUBA'
$DEFAULT_TAGGER = '20230311'

function Call1 {
    $args = $args -split ' '
    $cmd = $args[0]
    $path = Convert-Path $args[1]
    if ($args.count -gt 2) {
        $extraargs = $args[2..($args.count - 1)]
    }
    if ($cmd -eq 'open') {
        $runargs = '-p', '9284:9284', '-i'
    }
    $parentdir = Split-Path -Parent $path
    $filename = Split-Path -Leaf $path
    Write-Host "docker run $runargs -v ${parentdir}:/data $LARVATAGGER_IMAGE $cmd /data/$filename $extraargs"
    docker run $runargs -v ${parentdir}:/data $LARVATAGGER_IMAGE $cmd /data/$filename $extraargs
}

Switch ($args[0])
{
    default {
        Write-Host "Usage: larvatagger.ps1 open <filepath>"
        Write-Host "       larvatagger.ps1 import <filepath> [<outputfilename>]"
        Write-Host "       larvatagger.ps1 train <datarepository> <taggername>"
        Write-Host "       larvatagger.ps1 predict <datafile> <taggername>"
        Write-Host "       larvatagger.ps1 merge <filepath> [<outputfilename>]"
        Write-Host "       larvatagger.ps1 --more-help"
        Write-Host "       larvatagger.ps1 --version"
        Write-Host "       larvatagger.ps1 --update"
    }
    '--more-help' {
        Write-Host 'For most switches, arguments are passed to larvatagger.jl.'
        Write-Host 'larvatagger.jl extra arguments must come after larvatagger.ps1 arguments.'
        Write-Host 'Printing larvatagger.jl --help...'
        Write-Host ''

        docker run $LARVATAGGER_IMAGE --help
    }
    '--version' {
        docker run $LARVATAGGER_IMAGE --version
    }
    '-V' {
        docker run $LARVATAGGER_IMAGE --version
    }
    '--update' {
        Write-Host "docker pull flaur/larvatagger:latest"
        docker pull flaur/larvatagger:latest
    }
    'open' {
        Call1 $args
    }
    'import' {
        Call1 $args
    }
    'merge' {
        Call1 $args
    }
    'train' {
        $path = Convert-Path $args[1]
        $parentdir = Split-Path -Parent $path
        $dirname = Split-Path -Leaf $path
        if (Split-Path -IsAbsolute $args[2]) {
            $taggerpath = $args[2]
        } else {
            $taggerpath = Join-Path $PWD $args[2]
        }
        $modelsdir = Split-Path -Parent $taggerpath
        if (!(Test-Path -Path $modelsdir)) {
            New-Item -Path $modelsdir -ItemType 'directory'
        }
        $taggername = Split-Path -Leaf $taggerpath
        if ($args.count -gt 3) {
            $extraargs = $args[3..($args.count - 1)]
        }
        Write-Host "docker run --mount `"type=bind,src=$modelsdir,dst=/app/$TAGGING_BACKEND/models`" -v ${parentdir}:/data $LARVATAGGER_IMAGE train /app/$TAGGING_BACKEND /data/$dirname $taggername $extraargs"
        docker run --mount "type=bind,src=$modelsdir,dst=/app/$TAGGING_BACKEND/models" -v ${parentdir}:/data $LARVATAGGER_IMAGE train /app/$TAGGING_BACKEND /data/$dirname $taggername $extraargs
    }
    'predict' {
        $path = Convert-Path $args[1]
        if ($args.count -lt 3) {
            $taggername = $DEFAULT_TAGGER
        } else {
            $taggername = Split-Path -Leaf $args[2]
            if (!($taggername -eq $args[2])) {
                $modelsdir = Split-Path -Parent $args[2] | Convert-Path
                $runargs = '--mount', "type=bind,src=$modelsdir,dst=/app/$TAGGING_BACKEND/models"
            }
        }
        $parentdir = Split-Path -Parent $path
        $filename = Split-Path -Leaf $path
        if ($args.count -gt 3) {
            $extraargs = $args[3..($args.count - 1)]
        }
        Write-Host "docker run $runargs -v ${parentdir}:/data $LARVATAGGER_IMAGE predict /app/$TAGGING_BACKEND $taggername /data/$filename $extraargs"
        docker run $runargs -v ${parentdir}:/data $LARVATAGGER_IMAGE predict /app/$TAGGING_BACKEND $taggername /data/$filename $extraargs
    }
}
