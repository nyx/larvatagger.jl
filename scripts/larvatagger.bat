if not exist larvatagger.ps1 (
  PowerShell.exe -Command "Invoke-WebRequest https://gitlab.pasteur.fr/nyx/larvatagger.jl/-/raw/main/scripts/larvatagger.ps1?inline=false -OutFile larvatagger.ps1"
)
PowerShell.exe -ExecutionPolicy ByPass -File larvatagger.ps1 %*
