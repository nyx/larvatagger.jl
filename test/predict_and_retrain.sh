#!/usr/bin/env bash

# this script performs a few tests with the Docker image
# TODO: implement more use cases as in scenarii.sh

trap 'rm -rf "$TMPDIR"' EXIT

TMPDIR=$(mktemp -d) || exit 1
pushd "$TMPDIR"

# get wrapper script
wget -O larvatagger.sh https://gitlab.pasteur.fr/nyx/larvatagger.jl/-/raw/dev/scripts/larvatagger.sh?inline=false
chmod u+x larvatagger.sh

# get sample data
DATADIR1="data/FCF_attP2_1500062@UAS_Chrimson_Venus_X_0070/r_LED50_30s2x15s30s#n#n#n@100/20150701_105504"
mkdir -p "$DATADIR1"
wget -O chore_sample_output.tar.gz https://gitlab.pasteur.fr/nyx/artefacts/-/raw/master/PlanarLarvae/chore_sample_output.tar.gz?inline=false
tar zxf "$TMPDIR/chore_sample_output.tar.gz"
mv chore_sample_output/* "${DATADIR1}/"
rmdir chore_sample_output
DATADIR2="data/GMR_SS02113@UAS_Chrimson_Venus_X_0070/r_LED100_30s2x15s30s#n#n#n@100/20140918_170215"
mkdir -p "$DATADIR2"
wget -O trx_small.tgz https://gitlab.pasteur.fr/nyx/artefacts/-/raw/master/PlanarLarvae/trx_small.tgz?inline=false
cd "$DATADIR2"
tar zxf "$TMPDIR/trx_small.tgz"
cd "$TMPDIR"
rm -f *gz

# FIXME: podman cannot access /tmp

# use default tagger on Choreography data
time ./larvatagger.sh --gpus all predict "$(\ls -1 ${DATADIR1}/*.spine)"

head -n30 "${DATADIR1}/predicted.label"
echo '...'

# train a new tagger with the 2-data-file repository
time ./larvatagger.sh --gpus all train "data" newtagger --iterations 100

cat models/newtagger/clf_config.json

popd
