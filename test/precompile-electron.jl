using Blink

include(joinpath(@__DIR__, "precompile-common.jl")) # defines `srv`

win = Window(async=false)
loadurl(win, "http://127.0.0.1:9284")
close(win)

close(srv)
