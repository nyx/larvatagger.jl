#!/usr/bin/env bash

# Standalone script

# Behavior can be controlled using environment variables:
# * SLURM_OPTS: arguments for `srun`
# * CUDA_VISIBLE_DEVICES: can be set to -1 to disable GPU acceleration
# * KEEP_MODEL_FILES: can be set to 1 to have most of the generated files
#   (including training datasets and predicted labels) available in LarvaTagger/MaggotUBA/models
# * LOCAL_SCENARII: can be set to 1 to overwrite LarvaTagger.jl's test/scenarii.sh file
#   with the scenarii.sh file that comes along the present script (same directory)
# * LAUNCHER: default is "srun" if Slurm is available, otherwise nothing

# Example on Maestro (GPU-accelerated):
#   SLURM_OPTS='-p dbc_pmo -q dbc -c 4' JULIA_THREADS=4 ./deploy_and_test.sh
# or (CPU-only):
#   SLURM_OPTS='-p dedicated -q fast -c 16' JULIA_THREADS=16 ./deploy_and_test.sh

if ! [ -f scripts/install.sh ]; then
  echo "Call `basename $0` from the project's root directory:"
  echo "    test/deploy_and_test.sh"
  exit 1
fi

scripts/install.sh --uninstall
scripts/install.sh --with-backend --experimental

#############
## Maestro ##
#############

# SSL verification can be disabled for git clone to work:
#   export GIT_SSL_NO_VERIFY=true

# h5diff (for tests) on Maestro
if ! h5diff -V &> /dev/null; then
  module load hdf5 &> /dev/null
fi

if [ -n "$LAUNCHER" ]; then
  launcher="$LAUNCHER "
elif [ -n "$(which srun)" ]; then
  launcher="srun "
  echo "Slurm detected"
  if [ -z "$SLURM_OPTS" ]; then
    echo "Options can be passed to \`srun\` with variable SLURM_OPTS"
  else
    launcher="$launcher$SLURM_OPTS "
  fi
fi

#############

CURDIR=$(pwd)
LTROOT=$HOME/.local/share/larvatagger/LarvaTagger.jl

# shunit2 is for tests specifically
if [ -z "$(which shunit2)" ]; then
  (cd "$LTROOT/test" && \
    git clone -c advice.detachedHead=false -b v2.1.8 https://github.com/kward/shunit2)
fi

if [ -f LarvaTagger_test_data.tgz ]; then
  # To generate the test data, first run the tests with environment variable KEEP_MODEL_FILES=1
  # copy the LarvaTagger/MaggotUBA/models/test_train_* directories into LarvaTagger/LarvaTagger.jl/test/data
  # and build the archive from the LarvaTagger/LarvaTagger.jl directory.
  # For example:
  #   KEEP_MODEL_FILES=1 ./deploy_and_test.sh
  #   cd ~/.local/share/larvatagger/LarvaTagger/LarvaTagger.jl
  #   rm -rf test/data/test_train_*
  #   cp -Rp ../MaggotUBA/models/test_train_* test/data/
  #   tar zcvf LarvaTagger_test_data.tgz test/data/*
  (cd "$LTROOT" && tar zxvf LarvaTagger_test_data.tgz)
else
  # Not recommended; reproducibility is not guarantee across hosts or architectures yet
  (cd "$LTROOT" && \
    wget -O- https://dl.pasteur.fr/fop/ppk8GBQf/241127_LarvaTagger_test_data.tgz | tar zxv)
fi

if [ "$LOCAL_SCENARII" = "1" ]; then
  if [ -f "test/scenarii.sh" ]; then
    cp "test/scenarii.sh" "$LTROOT/test/"
  else
    echo "Cannot find a local scenarii.sh file"
  fi
fi

echo "${launcher}test/scenarii.sh"
(cd "$LTROOT" && ${launcher}test/scenarii.sh)
