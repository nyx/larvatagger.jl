datadir = joinpath(@__DIR__, "data")
isdir(datadir) || exit()
datafiles = String[joinpath(datadir, dir) for dir in readdir(datadir) if dir[1] != '.']
isempty(datafiles) && exit()
datafile = datafiles[1]
@info "Sample data file found" datafile

projectdir = dirname(Base.active_project())
include(joinpath(projectdir, "src/cli.jl"))

main([]; exit_on_error=false) # print help message
main(["open"]; exit_on_error=false) # print "File not found" error message
main(["import"]; exit_on_error=false) # print "File not found" error message
main(["predict"]; exit_on_error=false) # print "Missing positional arguments" error message
main(["train"]; exit_on_error=false) # print "Missing positional arguments" error message
srv = main(["open", datafile])
