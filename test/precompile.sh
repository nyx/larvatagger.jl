#!/bin/sh
root=$(dirname $(dirname $0))
if [ "$1" = "--shallow" ]; then

client=HTTP
julia="julia"
script="$root/test/precompile.jl"
packages="g++"

else

client=Blink
julia="xvfb-run julia"
script="$root/test/precompile-electron.jl"
# dependencies on Debian Bullseye and Ubuntu Focal
packages="unzip xvfb libgtk-3-0 libnss3 libxss1 libasound2 g++"

fi
apt-get update
apt-get install -y $packages

echo "Project root: $root"
$julia --project=$root -e "
using Pkg
Pkg.add([\"$client\",\"PackageCompiler\"])
using PackageCompiler
println(\"🢃 🢃 🢃 Includes logs from coverage script. Please ignore 🢃 🢃 🢃\")
create_sysimage(String[], sysimage_path=\"larvatagger.so\", precompile_execution_file=\"$script\",
                cpu_target=PackageCompiler.default_app_cpu_target())
println(\"🢁 🢁 🢁 Includes logs from coverage script. Please ignore 🢁 🢁 🢁\")
#Pkg.rm([\"PackageCompiler\",\"$client\"])"

apt-get autoremove -y $packages
apt-get clean
rm -rf /var/lib/apt/lists/*
