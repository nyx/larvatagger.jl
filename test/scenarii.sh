#!/usr/bin/env bash

# This script performs tests for the scripts/larvatagger-toolkit.jl command.
# See also the deploy_and_test.sh standalone script.

project_root=$(dirname "$(dirname "$(realpath "${BASH_SOURCE[0]}")")")

if [ -f "$project_root/test/shunit2" ]; then
  SHUNIT2=$(realpath "$project_root/test/shunit2")
elif [ -z "$(which shunit2)" ]; then
  echo "shUnit2 is required to run the tests (see https://github.com/kward/shunit2)"
  exit 1
else
  SHUNIT2=shunit2
fi

if ! h5diff -V &> /dev/null; then
  echo "h5diff (hdf5-tools package) is required to run the tests"
  exit 1
fi

if [ -z "$JULIA_PROJECT" ]; then
  export JULIA_PROJECT=$(realpath "$project_root/../TaggingBackends")
  echo "Setting JULIA_PROJECT=$JULIA_PROJECT"
fi

if nvidia-smi &> /dev/null; then
  if [ -z "$CUDA_VISIBLE_DEVICES" ] || [ "$CUDA_VISIBLE_DEVICES" -ge 0 ]; then
    # if [ -z "$CUBLAS_WORKSPACE_CONFIG" ]; then
    #   echo "Setting CUBLAS_WORKSPACE_CONFIG=:4096:8 to ensure reproducibility;"
    #   echo "see https://docs.nvidia.com/cuda/cublas/index.html#results-reproducibility"
    #   export CUBLAS_WORKSPACE_CONFIG=:4096:8
    # fi
    tagger_suffix=_gpu
  fi
fi

larvataggerjl="scripts/larvatagger-toolkit.jl"

datadir="test/data"
datapath="$project_root/$datadir"

maggotuba="../MaggotUBA"

seed=1028347112001

endTest() {
  echo "----------------------------------------------------------------------------"
}

prepareTestData() {
  tmpdir="$SHUNIT_TMPDIR/$1"
  shift
  rm -rf "$tmpdir"
  mkdir -p "$tmpdir"
  for file in sample.spine sample.outline $@; do
    cp "$datapath/$file" "$tmpdir/" || exit 1
  done
  echo $tmpdir
}

prepareTrainingData() {
  labelfile=$1
  shift
  tmpdir="$SHUNIT_TMPDIR/$1"
  shift
  extrafiles="sample.spine sample.outline $@"
  rm -rf "$tmpdir"
  mkdir -p "$tmpdir"
  for file in $extrafiles $labelfile; do
    cp "$datapath/$file" "$tmpdir/" || exit 1
  done
  echo $tmpdir
}

# requires: partial_predictions.label
testImportLabelFile() {
  filename=result_imported.label
  # run
  cd "$project_root"
  echo "\"$larvataggerjl\" import \"$datadir/partial_predictions.label\" $filename --default-label=\"not back-up\""
  "$larvataggerjl" import "$datadir/partial_predictions.label" $filename --default-label="not back-up"
  # test
  assertTrue '\`import\` failed to reproduce the imported.label file' '$(cmp "$datapath/imported.label" "$datapath/$filename")'
  # clean up
  rm -f "$datapath/$filename"
  endTest
}

# requires: cropped.label
testCropTracks() {
  filename=result_cropped.label
  # run
  cd "$project_root"
  echo "\"$larvataggerjl\" import \"$datadir/trx.mat\" $filename --copy-labels --segment=59,62"
  "$larvataggerjl" import "$datadir/trx.mat" $filename --copy-labels --segment=59,62
  # test
  assertTrue '\`import\` failed to reproduce the cropped.label file' '$(cmp "$datapath/cropped.label" "$datapath/$filename")'
  # clean up
  rm -f "$datapath/$filename"
  endTest
}

# requires: sample.spine sample.outline test_train_default/predicted.label
testPredictDefault() {
  tagger="test_train_default$tagger_suffix"
  tmpdir=$(prepareTestData $tagger)
  # restore the model
  mkdir -p "$maggotuba/models"
  rm -rf "$maggotuba/models/$tagger"
  echo "cp -ra \"$datapath/$tagger\" \"$maggotuba/models/\""
  cp -ra "$datapath/$tagger" "$maggotuba/models/"
  [ -f "$maggotuba/models/$tagger/clf_config.json" ] || exit 1
  # run
  cd "$project_root"
  echo "\"$larvataggerjl\" predict \"$maggotuba\" $tagger \"$tmpdir/sample.spine\" --debug"
  "$larvataggerjl" predict "$maggotuba" $tagger "$tmpdir/sample.spine" --debug
  # compare
  filename=predicted.label
  predictions="$tmpdir/$filename"
  expected_labels="$datapath/$tagger/$filename"
  assertFalse "\`predict\` failed to generate file $filename" '[ -z "$predictions" ]'
  assertTrue "\`predict\` failed to reproduce file $filename" '$(cmp "$expected_labels" "$predictions")'
  endTest
}

# requires: sample.spine sample.outline trx.mat original_predictions.label test_train_default/predicted.label
testPredictWithConflictingInput() {
  tagger="test_train_default$tagger_suffix"
  # make test data
  touch "$datapath/spurious.txt"
  tmpdir=$(prepareTestData $tagger trx.mat original_predictions.label spurious.txt)
  # restore the model
  mkdir -p "$maggotuba/models"
  rm -rf "$maggotuba/models/$tagger"
  echo "cp -Rp \"$datapath/$tagger\" \"$maggotuba/models/\""
  cp -Rp "$datapath/$tagger" "$maggotuba/models/"
  [ -f "$maggotuba/models/$tagger/clf_config.json" ] || exit 1
  # run
  cd "$project_root"
  echo "\"$larvataggerjl\" predict \"$maggotuba\" $tagger \"$tmpdir\" --debug"
  "$larvataggerjl" predict "$maggotuba" $tagger "$tmpdir" --debug
  # compare
  filename=predicted.label
  predictions="$tmpdir/$filename"
  expected_labels="$datapath/$tagger/$filename"
  assertFalse "\`predict\` failed to generate file $filename" '[ -z "$predictions" ]'
  assertTrue "\`predict\` failed to reproduce file $filename" '$(cmp "$expected_labels" "$predictions")'
  # clean up
  rm -f "$datapath/spurious.txt"
  endTest
}

# requires: sample.spine sample.outline original_predictions.label test_train_default/*
testTrainDefault() {
  tagger="test_train_default$tagger_suffix"
  tmpdir=$(prepareTrainingData original_predictions.label $tagger)
  # run
  cd "$project_root"
  echo "\"$larvataggerjl\" train \"$maggotuba\" \"$tmpdir\" $tagger --seed $seed --debug"
  "$larvataggerjl" train "$maggotuba" "$tmpdir" $tagger --seed $seed --debug
  # test
  postTrain $tagger
  endTest
}

# requires: sample.spine sample.outline imported.label test_train_one_class/*
testTrainOneClass() {
  tagger="test_train_one_class$tagger_suffix"
  tmpdir=$(prepareTrainingData imported.label $tagger)
  # run
  cd "$project_root"
  echo "\"$larvataggerjl\" train \"$maggotuba\" \"$tmpdir\" $tagger --iterations 10 --seed $seed --labels=\"back-up,not back-up\""
  "$larvataggerjl" train "$maggotuba" "$tmpdir" $tagger --iterations 10 --seed $seed --labels="back-up,not back-up"
  # test
  postTrain $tagger
  endTest
}

# requires: sample.spine sample.outline imported.label test_train_one_class_with_weights/*
testTrainOneClassWithWeights() {
  tagger="test_train_one_class_with_weights$tagger_suffix"
  tmpdir=$(prepareTrainingData imported.label $tagger)
  # run (compared with testTrainOneClass, we also swap the labels)
  cd "$project_root"
  echo "\"$larvataggerjl\" train \"$maggotuba\" \"$tmpdir\" $tagger --iterations 10 --seed $seed --labels=\"not back-up,back-up\"" --class-weights 1,10
  "$larvataggerjl" train "$maggotuba" "$tmpdir" $tagger --iterations 10 --seed $seed --labels="not back-up,back-up" --class-weights 1,10
  # test
  postTrain $tagger
  endTest
}

# requires: sample.spine sample.outline gui_imported.label test_train_one_class_with_encoder/*
testTrainOneClassWithEncoder() {
  tagger="test_train_one_class_with_encoder$tagger_suffix"
  tmpdir=$(prepareTrainingData gui_imported.label $tagger trx.mat)
  # run
  cd "$project_root"
  echo "\"$larvataggerjl\" train \"$maggotuba\" \"$tmpdir\" $tagger --iterations 10 --seed $seed --labels=\"hunch,¬hunch\" --pretrained-model=20230524-hunch-25 --balancing-strategy=maggotuba"
  "$larvataggerjl" train "$maggotuba" "$tmpdir" $tagger --iterations 10 --seed $seed --labels="hunch,¬hunch" --pretrained-model=20230524-hunch-25 --balancing-strategy=maggotuba
  # test
  postTrain $tagger
  endTest
}

# requires: sample.spine sample.outline trx.mat gui_imported.label original_predictions.label test_train_selected_files/*
testTrainSelectedFiles() {
  tagger="test_train_selected_files$tagger_suffix"
  tmpdir=$(prepareTrainingData gui_imported.label $tagger trx.mat original_predictions.label partial_predictions.label)
  mkdir "$tmpdir/subdir"; mv $tmpdir/*.label "$tmpdir/subdir/"; mv $tmpdir/trx.mat "$tmpdir/subdir/"
  echo "(cd \"$tmpdir\" && find . -name gui_imported.label -print > filelist.txt)"
  (cd "$tmpdir" && find . -name gui_imported.label -print > filelist.txt)
  # run
  cd "$tmpdir"
  echo "\"$project_root/$larvataggerjl\" train \"$project_root/$maggotuba\" filelist.txt $tagger --seed $seed --labels=\"run_large,cast_large,hunch_large\" --balancing-strategy=maggotuba --iterations=10"
  "$project_root/$larvataggerjl" train "$project_root/$maggotuba" filelist.txt $tagger --seed $seed --labels="run_large,cast_large,hunch_large" --balancing-strategy=maggotuba --iterations=10
  # test
  cd "$project_root"
  postTrain $tagger
  endTest
}

# requires: sample.spine sample.outline trx.mat gui_imported.label original_predictions.label test_train_recursive_selection/*
testTrainRecursiveSelection() {
  tagger="test_train_recursive_selection$tagger_suffix"
  tmpdir=$(prepareTrainingData gui_imported.label $tagger trx.mat original_predictions.label partial_predictions.label)
  mkdir "$tmpdir/subdir"; mv $tmpdir/*.label "$tmpdir/subdir/"; mv $tmpdir/trx.mat "$tmpdir/subdir/"
  # run
  cd "$project_root"
  echo "\"$larvataggerjl\" train \"$maggotuba\" \"$tmpdir/**/gui_imported.label\" $tagger --seed $seed --labels=\"run_large,cast_large,hunch_large\" --balancing-strategy=maggotuba --iterations=10"
  "$larvataggerjl" train "$maggotuba" "$tmpdir/**/gui_imported.label" $tagger --seed $seed --labels="run_large,cast_large,hunch_large" --balancing-strategy=maggotuba --iterations=10
  # test
  postTrain $tagger
  endTest
}

postTrain() {
  tagger=$1
  expected_tagger_dir="$datapath/$tagger"
  tagger_dir="$project_root/$maggotuba/models/$tagger"
  interim_dir="$maggotuba/data/interim/$tagger"

  # interim files
  filename=larva_dataset.hdf5
  larva_dataset="$interim_dir/$(cd "$interim_dir"; ls larva_dataset_*.hdf5)"
  assertFalse "\`train\` failed to generate file $filename" '[ -z "$larva_dataset" ]'
  # h5diff output is filtered to ignore the absolute file paths in attributes
  assertTrue "\`train\` failed to reproduce file $filename" '[ -z "$(h5diff "$expected_tagger_dir/$filename" "$larva_dataset" | grep -v " differences found" | grep -v "attribute: <path of </samples/sample_")" ]'
  if [ "$KEEP_MODEL_FILES" = "1" ]; then
    cp "$larva_dataset" "$tagger_dir/$filename"
  fi

  # test model files
  enc_weights=retrained_encoder.pt
  enc_config=autoencoder_config.json
  clf_weights=trained_classifier.pt
  clf_config=clf_config.json
  enc_weights_path="$tagger_dir/$enc_weights"
  enc_config_path="$tagger_dir/$enc_config"
  clf_weights_path="$tagger_dir/$clf_weights"
  clf_config_path="$tagger_dir/$clf_config"
  assertFalse "\`train\` failed to generate file $enc_weights" '[ -z "$enc_weights_path" ]'
  assertFalse "\`train\` failed to generate file $enc_config" '[ -z "$enc_config_path" ]'
  assertFalse "\`train\` failed to generate file $clf_weights" '[ -z "$clf_weights_path" ]'
  assertFalse "\`train\` failed to generate file $clf_config" '[ -z "$clf_config_path" ]'
  assertTrue "\`train\` failed to reproduce file $enc_weights" '$(cmp "$expected_tagger_dir/$enc_weights" "$enc_weights_path")'
  assertTrue "\`train\` failed to reproduce file $enc_config" '$(cmp "$expected_tagger_dir/$enc_config" "$enc_config_path")'
  assertTrue "\`train\` failed to reproduce file $clf_weights" '$(cmp "$expected_tagger_dir/$clf_weights" "$clf_weights_path")'
  assertTrue "\`train\` failed to reproduce file $clf_config" '[ -z "$(diff "$expected_tagger_dir/$clf_config" "$clf_config_path" | grep -E "^[<>]   " | grep -vE larva_dataset_.*[.]hdf5)" ]'

  # test the predictions
  echo "\"$larvataggerjl\" predict \"$maggotuba\" $tagger \"$tmpdir/sample.spine\""
  "$larvataggerjl" predict "$maggotuba" $tagger "$tmpdir/sample.spine"
  predictions="$tmpdir/predicted.label"
  expected_labels="$datapath/$tagger/predicted.label"
  assertTrue "\`predict\` failed to reproduce file predicted.label" '$(cmp "$expected_labels" "$predictions")'

  # clean up
  if [ "$KEEP_MODEL_FILES" = "1" ]; then
    echo "Interim files in: $tagger_dir"
    cp "$predictions" "$tagger_dir/"
  else
    rm -rf "$tagger_dir"
  fi
}

. $SHUNIT2
