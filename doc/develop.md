*LarvaTagger* is an umbrella project for the UI *LarvaTagger.jl*, the default *MaggotUBA*-based tagging backends and other related *Julia* and *Python* projects.

# Project map

<img src="project_structure.svg" />

The *TidyObservables.jl* and *ObservationPolicies.jl* are hosted on [gitlab.com](https://gitlab.com/dbc-nyx).

## Deployment projects

More related projects can be found at [gitlab.com/LarvaTaggerPipelines](https://gitlab.com/larvataggerpipelines).
For example, the [Pesticides](https://gitlab.com/larvataggerpipelines/pesticides), [Alzheimer](https://gitlab.com/larvataggerpipelines/Alzheimer), [t5_analysis_replicates](https://gitlab.com/larvataggerpipelines/t5_analysis_replicates) and [TrxmatRetagger](https://gitlab.com/larvataggerpipelines/TrxmatRetagger) projects are processing pipelines that include *LarvaTagger.jl* with *MaggotUBA*-based tagger.

They all rely on a *Docker* image of *LarvaTagger* and automate the deployment on specific HPC clusters using *make*.

# Tests

Only three sub-projects only include automatic tests.

On the *Julia* side, the lower-level functionalities are provided by the *PlanarLarvae.jl* project. This project features unit tests with high coverage, that are run on every commit.

Similarly, the *TidyObservables.jl* project has unit tests and a GitLab workflow to run these tests on every commit.

For the remaining part of the *LarvaTagger* project, high-level functional tests only are available.
These tests are available in the *LarvaTagger.jl* project, in the test directory, file *scenarii.sh*. They depend on [*shUnit2*](https://github.com/kward/shunit2).
