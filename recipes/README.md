# *Docker* images

*Docker* images can be used with the *docker* command.
To make their usage easier, standalone scripts are available in the *scripts* directory.
They can be used with no installation step other than setting up *Docker*.

Windows users should use the [larvatagger.bat script](#the-larvataggerbat-script), while macOS and Linux users should use the [larvatagger.sh script](#the-larvataggersh-script) instead.

The first two sections describe how to use these scripts.
The last section is dedicated to *Singularity*/*Apptainer*.

## The *larvatagger.bat* script

### Getting the script

The *larvatagger.bat* script is available in the *scripts* directory of the project.

If you do not have a copy of the project, you can simply [download the script](https://gitlab.pasteur.fr/nyx/larvatagger.jl/-/raw/main/scripts/larvatagger.bat?inline=false) and save it in your `C:\Users\<user>` directory.

Note that *Docker* still has to be installed.

In the command prompt (`cmd`), to check the script works properly, try:
```
larvatagger.bat --version
```

The first time the script runs, *Docker* downloads the latest image.

### Launching the GUI

To open a track data file, try:
```
larvatagger.bat open Z:\path\to\datafile
```
This turns the command-line into a *Julia* interpreter to serve the app as a web server.

As instructed, open [http://localhost:9284](http://127.0.0.1:9284) in your web browser to access the app.

You can stop the server in the command-line pressing Ctrl+C.
The server can also be exited typing `exit()` and pressing Enter.

The `--browser` argument mentioned elsewhere does not work with the *larvatagger.bat* script yet.

A default tagger will be available in the app.

### Automatic tagging

To apply a tagger (e.g. *20230311*) to a track data file, type:
```
larvatagger.bat predict Z:\path\to\datafile 20230311
```
This will create a *predicted.label* in the same directory as the data file. If the file already exists, it is overwritten.

To apply a custom tagger, named *e.g.* *mytagger* and stored in the `C:\Users\<user>\models\mytagger` directory, type instead:
```
larvatagger.bat predict Z:\path\to\datafile models\mytagger
```
Here, `models\mytagger` is the relative path of the directory of the tagger.
This path has to include at least one `\` character so that it is identified as an external tagger.
Otherwise, *LarvaTagger.jl* would look up for the tagger inside the *Docker* container, and fail to find it.

### More to come...

The `train`, `merge` and `import` commands work similarly to the *larvatagger.sh* script. See the below section to know more about these commands.

Not all arguments and options are available, though. See also `larvatagger.bat --help`.


## The *larvatagger.sh* script

### Getting the script

The *larvatagger.sh* script is available in the *scripts* directory of the project.

If you do not have a copy of the project, you can simply [download the script](https://gitlab.pasteur.fr/nyx/larvatagger.jl/-/raw/main/scripts/larvatagger.sh?inline=false) and set its permissions so that it can be executed.

Note that *Docker* still has to be installed.

To check the script works properly, in a terminal, change directory to where the *larvatagger.sh* script is located and try:
```
./larvatagger.sh --version
```

The first time the script runs, *Docker* downloads the latest image.

#### Compatibility issue on macOS

Some combinations of Apple architectures and *Docker* versions will hang and complain with the following message:
```
WARNING: The requested image's platform (linux/amd64) does not match the detected host platform (linux/arm64/v8) and no specific platform was requested
```
This can be fixed using *Docker Desktop* and enabling [Rosetta 2 virtualization (see section “How to run x86 containers on Apple Mac M1 Pro”)](https://collabnix.com/warning-the-requested-images-platform-linux-amd64-does-not-match-the-detected-host-platform-linux-arm64-v8/).

### Launching the GUI

To open a track data file, try:
```
./larvatagger.sh open path/to/datafile
```
This turns the command-line into a *Julia* interpreter to serve the app as a web server.

As instructed, open [http://localhost:9284](http://127.0.0.1:9284) in your web browser to access the app.
As similarly instructed, you can stop the server in the command-line pressing Ctrl+D.
The server can also be exited typing `exit()` and pressing Enter.

A default tagger will be available in the app.

### Automatic tagging

Behavior labels files (*.json* files with extension *.label) can be generated using the CLI with:
```
./larvatagger.sh predict path/to/data/file
```

Note: since `TaggingBackends==0.10`, argument `--skip-make-dataset` is default behavior; pass `--make-dataset` instead to enforce the former default.

Similarly, all the track data files in a directory tree can be processed with a single command:
```
./larvatagger.sh predict path/to/data/repository
```

Training a new tagger is almost as easy:
```
./larvatagger.sh train path/to/data/repo my-tagger
```
However, for the trained models to be persistant, the `train` command must store them outside the container.
Per default, `larvatagger.sh train` creates a *models* directory in the current directory and stores the model instances in there.

As a consequence, the `predict` command can use external model instances passing paths instead of instance names.
To distinguish between paths and names, *larvatagger.sh* checks whether the corresponding directory exists. If a directory exists at the path pointed to by `--model-instance` (on the host), then it is mounted into the container as an external trained model:
```
./larvatagger.sh predict path/to/data/file --model-instance models/my-tagger
```

To enable GPU acceleration, pass the `--gpus all` argument.

To check whether GPUs are detected, run `docker run --rm --gpus all --entrypoint checkgpu flaur/larvatagger`. This command should print a list of your GPUs, otherwise prints *cuda unavailable*.

The `train` and `predict` switches may require the *realpath* core utility that is not available per default on macOS. macOS users may need to install *coreutils*: `brew install coreutils`.

### Building an image

The *larvatagger.sh* script features a `build` command to build a local *Docker* image:
```
scripts/larvatagger.sh build
```

Optionally, you can also get the default backend (currently *20230311*) with:
```
scripts/larvatagger.sh build --with-default-backend
```
Currently, Docker images on Docker Hub are built with:
```
scripts/larvatagger.sh --target confusion build --with-default-backend
```

If you want another tagger, *e.g.* the *20230129* tagger implemented by the *20230129* branch of the *MaggotUBA-adapter* repository, do:
```
scripts/larvatagger.sh build --with-backend MaggotUBA/20230129
```

The `build` command requires a local copy of the *LarvaTagger.jl* project. In addition, the *larvatagger.sh* script must be executed from the project root directory, hence the path *scripts/larvatagger.sh*.

## Using the *docker* command

The present section gives low-level usage examples.

### Getting available images

Images are published on [Docker Hub](https://hub.docker.com/r/flaur/larvatagger/tags).

They ship with *LarvaTagger.jl* and, optionally, with a *MaggotUBA*-based backend for automatic tagging.

Try:
```
docker pull flaur/larvatagger
```

Beware that images that ship with a tagging backend are relatively large files (>5GB on disk).
If you are not interested in automatic tagging, use the `flaur/larvatagger:0.19-standalone` image instead.

### Upgrading

The `docker pull flaur/larvatagger` command can be used to upgrade the *Docker* image as well.

It should be safe to upgrade but beware that, for example, the default tagger may change.

The last release known to include a specific tagger will stay available on Docker Hub.

### Launching the GUI

A number of options must be passed to the *docker* command for the container to open a data file in the current working directory:

```
docker -iv $(pwd):/data -p 9284:9284 flaur/larvatagger open <yourfilename>
```
with `<yourfilename>` the name of your data file.

In *PowerShell* on Windows, use `${PWD}` instead of `$(pwd)`.

In the command above, `/data` represents the local directory, as made available within the container.
This is required, since no files outside the container can be accessed.

The port *LarvaTagger.jl* listens to must also be exported with `-p 9284:9284` so that it can be accessed from outside the container.

### `train` and `predict`

Just like the *larvatagger* script, the docker image admits more commands/switches, including `import`, `train` and `predict`, or options such as `--help`.
For these other commands, neither `-i` nor `-p 9284:9284` are necessary.

See the main [README](https://gitlab.pasteur.fr/nyx/larvatagger.jl#automatic-tagging) for usage information.

Additionally, for the `train` switch, an external *models* directory must be explicitly mounted */app/MaggotUBA/models*.
Ownership of the generated files will have to be fixed (*root* per default on Unix-like systems).

### Windows *PowerShell* example

Using a few [sample files](https://gitlab.pasteur.fr/nyx/artefacts/-/tree/master/PlanarLarvae):
```
PS D:\> ls | Select Name

Name
----
trx.mat
trx_small.mat


PS D:\> mkdir models
```
We train a *test-tagger* model instance for the *MaggotUBA* backend shipped with image *flaur/larvatagger:latest*:
```
PS D:\> docker run -v ${PWD}:/data --mount type=bind,src=D:/models,dst=/app/MaggotUBA/models flaur/larvatagger train /app/MaggotUBA /data/trx.mat test-tagger --labels run_large,cast_large,back_large,stop_large --layers 2 --balancing-strategy maggotuba
┌ Info: Pushing file to backend
│   backend = "MaggotUBA"
│   instance = "test-tagger"
└   file = "/data/trx.mat"
WARNING: could not import HDF5.exists into MAT
INFO:main: running make_dataset.py
[ Info: Counting behavior labels in run: test-tagger
┌ Info: Sample sizes (observed, selected):
│   stop_large = (392, 392)
│   cast_large = (30339, 784)
│   run_large = (68764, 784)
└   back_large = (1818, 392)
[ Info: Sampling series of spines in run: test-tagger
INFO:main: running train_model.py
DEBUG:train_model: trailing arguments: {'layers': '2'}
PS D:\> ls models/test-tagger | Select Name

Name
----
autoencoder_config.json
best_validated_encoder.pt
clf_config.json
retrained_encoder.pt
trained_classifier.pt

```
This results in a new directory in *models*.

We now tag a second data file using the *test-tagger* instance:
```
PS D:\> docker run -v ${PWD}:/data --mount type=bind,src=D:/models,dst=/app/MaggotUBA/models flaur/larvatagger predict /app/MaggotUBA test-tagger /data/trx_small.mat
┌ Info: Pushing file to backend
│   backend = "MaggotUBA"
│   instance = "test-tagger"
└   file = "/data/trx_small.mat"
WARNING: could not import HDF5.exists into MAT
INFO:main: running predict_model.py
PS D:\> ls | Select Name

Name
----
models
predicted.label
trx.mat
trx_small.mat

```
A *predicted.label* file was generated.
This file can be loaded in the *LarvaTagger.jl* viewer like any *table.csv*, *.mat* or *.outline* file.

## *Singularity*/*Apptainer*

*Docker* is usually not available on shared HPC clusters.
[*Singularity*](https://sylabs.io/) or [*Apptainer*](http://apptainer.org/) are used instead, and the *Docker* image for *LarvaTagger.jl* can be run using these technologies.

An issue with the `predict` and `train` switches makes the backend unable to use the internal */app/MaggotUBA/data* directory to organise the *raw*, *interim* and *processed* data files.
A workaround consists in using an external *apptainer-data* directory and bind mount it as */app/MaggotUBA/data*.
For example:

```
apptainer run --bind apptainer-data:/app/MaggotUBA/data docker://flaur/larvatagger predict /app/MaggotUBA 20230311 path/to/datafile --data-isolation
```

Depending on where the data file is located in the file system, the parent directory may also need to be bind-mounted.

Running *Docker* containers may not be accessible from worker nodes. A workaround consists in converting the *Docker* image into a *Singularity* image file for *Apptainer*, and then call `apptainer run` for that *.sif* image.

See also a [deployment example](https://gitlab.com/larvataggerpipelines/t5_analysis_replicates/-/blob/0db595945b6c2d1bdf7147b136807b11c41d52dc/Makefile.maestro#L53), and [other examples](https://gitlab.pasteur.fr/nyx/larvatagger.jl/-/blob/dev/doc/develop.md?ref_type=heads#deployment-projects).
