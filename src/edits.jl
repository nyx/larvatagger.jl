
abstract type Transaction end
abstract type TagDefinition <: Transaction end
abstract type Tagging <: Transaction end

struct CreateTag <: TagDefinition
    name
end

struct RenameTag <: TagDefinition
    change::Pair
end

""" Record the deletion of a tag in the table.

!! note

    Not to be confused with [`RemoveTag`](@ref).

"""
struct DeleteTag <: TagDefinition
    name
end

struct ActivateTag <: TagDefinition
    name
end

struct DeactivateTag <: TagDefinition
    name
end

struct ChangeTagColor <: TagDefinition
    change::Pair{String, String}
end

struct AddTag <: Tagging
    larva::LarvaID
    timestep::Int
    name
end

""" Deselect a tag at a time point of a track.

!! note

    Not to be confused with [`DeleteTag`](@ref).

"""
struct RemoveTag <: Tagging
    larva::LarvaID
    timestep::Int
    name
end

struct OverrideTags <: Tagging
    larva::LarvaID
    timestep::Union{Int, Tuple{Int, Int}}
    replacementtag
    previoustags
end

struct RemoveAllTags <: Tagging
    larva::LarvaID
    timestep::Union{Int, Tuple{Int, Int}}
    previoustags
end
