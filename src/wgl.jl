getsession(c) = gethub(c)[:session]
setsession!(c, session) = (gethub(c)[:session] = session)
using Makie: label_info

const r = Bonito.jsrender

const LarvaTaggerJS = Bonito.Asset(joinpath(@__DIR__, "larvatagger.js"))
const LarvaTaggerCSS = Bonito.Asset(joinpath(@__DIR__, "larvatagger.css"))
const LoadAwesomeCSS = Bonito.Asset(joinpath(@__DIR__, "loadawesome.css"))

scrollable(elements...) = DOM.div(DOM.table(elements...);
                                  class="scrollable", scrolling="auto")

function with_attributes(kwargs; class="", style="", kwargs′...)
    attributes = Dict{Symbol, Any}(kwargs′)
    merge!(attributes, Dict{Symbol, Any}(kwargs))
    if !isempty(class)
        existing = get(attributes, :class, "")
        attributes[:class] = isempty(existing) ? class : join((class, existing), " ")
    end
    if !isempty(style)
        existing = get(attributes, :style, "")
        if isempty(existing)
            style = endswith(style, ";") ? style : style * ";"
        else
            style = join((style, existing), endswith(style, ";") ? " " : "; ")
        end
        attributes[:style] = style
    end
    return attributes
end

css_button = "button"

struct ControlPanel
    id::String
    title::String
    active::Bool
    elements
    attributes
    dom_id
end

function ControlPanel(id::String, title::String, active::Bool, elements, attributes)
    ControlPanel(id, title, active, elements, attributes, dom_id())
end

function ControlPanel(elements...; id::String, title::String, active::Bool=false, attributes...)
    attributes = with_attributes(attributes; class="control-panel")
    ControlPanel(id, title, active, elements, attributes)
end

function lowerdom(panel::ControlPanel)
    tabid = "select-" * panel.id
    escaped_attrs = (:for=>tabid,)
    return DOM.div(DOM.input(; type="radio", name="tab", id=tabid, class="cp-tab-switch",
                             checked=panel.active),
                   DOM.label(panel.title; class="cp-tab-label", escaped_attrs...),
                   DOM.div(panel.elements...; id=panel.id, panel.attributes...);
                   class="cp-tab")
end
Bonito.jsrender(session::Session, panel::ControlPanel) = lowerdom(session, panel)

cp(panels...) = DOM.div(panels...; class="cp-tabs")

function homebutton()
    NyxWidgets.Button("⌂";
                      class=css_button,
                      style="position:absolute; top:1.5625rem; right:2.5rem;")
end

struct AssayPlot
    controller
    figure::Figure
    axis::Axis
    larvae::DecoratedLarvae
    homebutton::NyxWidgets.Button
    dom_id
end

function AssayPlot(controller, figure::Figure, axis::Axis, larvae::DecoratedLarvae,
        homebutton::NyxWidgets.Button)
    AssayPlot(controller, figure, axis, larvae, homebutton, dom_id())
end

function AssayPlot(ctrl, larvae::DecoratedLarvae; size=FIGSIZE)
    gethub(ctrl)[:decoratedlarvae] = larvae
    fig = Figure(size=size)
    width = 0.1f0 # try to get 1px
    color = RGBAf(0, 0, 0, 0.36)
    ax = Axis(fig.layout[1, 1], aspect=DataAspect(), xgridwidth=width, ygridwidth=width,
              xgridcolor=color, ygridcolor=color)
    autosize!(ax, size)
    if !isempty(larvae)
        plot = larvaplot!(ax, larvae)
        setkeyboardevents!(plot.parent, getplayer(ctrl))
        setbounds!(ax, ctrl, larvae)
    end
    button = homebutton()
    on(button) do _
        setbounds!(ax, ctrl, larvae)
    end
    AssayPlot(ctrl, fig, ax, larvae, button)
end

function AssayPlot(ctrl, larvae::Vector{LarvaModel}, visible::AbstractObservable{Bool};
        downsampling_threshold=1e5, kwargs...)
    npoints = sum([length(larva.path) for larva in larvae])
    if downsampling_threshold <= npoints
        @info "Trajectory down-sampling active" npoints
        sample = downsampler()
        larvae = sample.(larvae)
    end
    plot = AssayPlot(ctrl,
                     DecoratedLarvae(larvae,
                                     gettags(ctrl),
                                     gettimestep(ctrl),
                                     visible);
                     kwargs...)
    return plot
end

function AssayPlot(ctrl, larvae::Dict{LarvaID, LarvaModel}, args...; kwargs...)
    AssayPlot(ctrl, collect(values(larvae)), args...; kwargs...)
end

function assayplot(data, ctrl, args...; kwargs...)
    plot = AssayPlot(ctrl, data, args...; kwargs...)
    setmouseevents!(plot; consume=true)
    return plot
end

lowerdom(p::AssayPlot) = DOM.div(p.homebutton, p.figure; class="relative")
Bonito.jsrender(session::Session, p::AssayPlot) = lowerdom(session, p)

function setmouseevents!(plot::AssayPlot; kwargs...)
    setmouseevents!(plot.axis.scene, plot.larvae, plot.controller; kwargs...)
end

struct TrackPlot
    controller
    figure
    axis::Axis
    larvae::Vector{LarvaModel}
    view::SingleLarvaView
    homebutton::NyxWidgets.Button
    dom_id
end

function TrackPlot(controller, figure, axis::Axis, larvae::Vector{LarvaModel},
        view::SingleLarvaView, homebutton::NyxWidgets.Button)
    TrackPlot(controller, figure, axis, larvae, view, homebutton, dom_id())
end

function TrackPlot(controller, larvae::Vector{LarvaModel};
        size=FIGSIZE, editabletags::Bool=true)
    view = SingleLarvaView(larvae, controller; editabletags=editabletags)
    fig = Figure(size=size)
    width = 0.1f0 # try to get 1px
    color = RGBAf(0, 0, 0, 0.36)
    ax = Axis(fig.layout[1, 1], aspect=DataAspect(), xgridwidth=width, ygridwidth=width,
              xgridcolor=color, ygridcolor=color)
    autosize!(ax, size)
    larvaplot!(ax, view)
    setbounds!(ax, controller, larvae)
    on(getactivelarva(controller)) do id
        if !isnothing(id)
            for larva in larvae
                if id == larva.id
                    setbounds!(ax, controller, larva)
                    settimebounds!(controller, larva)
                    break
                end
            end
        end
    end
    button = homebutton()
    on(button) do _
        deactivatelarva!(controller)
    end
    TrackPlot(controller, fig, ax, larvae, view, button)
end

TrackPlot(ctrl, model::Dict{<:Integer, LarvaModel}, args...; kwargs...) = TrackPlot(ctrl, collect(values(model)), args...; kwargs...)

trackplot(model, controller; kwargs...) = TrackPlot(controller, model; kwargs...)

lowerdom(p::TrackPlot) = DOM.div(p.homebutton, p.figure; class="relative")
Bonito.jsrender(session::Session, p::TrackPlot) = lowerdom(session, p)

player(times::Vector; kwargs...) = player(timecontroller(times); kwargs...)

player(controller; kwargs...) = player(getplayer(controller); kwargs...)

function player(animator::TimeController; kwargs...)
    step = timestep(animator)
    stepmin = animator.stepmin
    stepmax = animator.stepmax
    playbackspeeds = ["0.25", "0.5", "0.75", "1.0", "1.25", "1.5", "2"]
    view = PlayerView(animator, playbackspeeds)
    oninput = js"""(evt) => {
        var step = evt.srcElement.value;
        LarvaTagger.timeSlider(evt.srcElement, $step, $stepmin, $stepmax).moveto(step)
    }"""
    # set oninput attribute in immutable structs
    slider = view.timeslider
    slider = Players.TimeSlider(slider.player, slider.index, slider.playpause, oninput,
                                slider.attributes, view.dom_id)
    view = PlayerView(view.player, view.playpause, view.stepforward, view.stepbackward,
                      view.repeat, view.speedselector, slider, view.timelabel,
                      view.attributes, view.dom_id)
    return view
end

struct AssayViewer
    plot::AssayPlot
    player::PlayerView
    visible::AbstractObservable{Bool}
    dom_id
end

function assayviewer(controller; kwargs...)
    model = getlarvae(controller)
    visible = Observable(true)
    plot = assayplot(model, controller, visible; kwargs...)
    play = player(controller)
    on(visible) do b
        if b
            # refresh view
            notify(gettimestep(controller))
        end
    end
    AssayViewer(plot, play, visible, dom_id())
end

function lowerdom(av::AssayViewer)
    focus = js"(evt)=>{ LarvaTagger.focusOnTimeSlider(evt.srcElement); }"
    return DOM.div(av.plot, av.player; class="flex flex-col", onmouseenter=focus)
end
Bonito.jsrender(session::Session, av::AssayViewer) = lowerdom(session, av)

struct LarvaInfo
    controller
    id::LarvaID
    hovered::AbstractObservable{Bool}
    clicked::AbstractObservable{Bool}
    reviewed::AbstractObservable{Bool}
    edited::AbstractObservable{Bool}
    included::AbstractObservable{Bool}
    dom_id
end

function larvainfo(controller, id)
    hovered = Observable(false)
    clicked = Observable(false)
    reviewed = Observable(false)
    edited = Observable(false)
    #
    larvae = gethub(controller)[:decoratedlarvae]
    larvaindex = nothing
    for (i, larva) in enumerate(larvae.larvae)
        larva = larva.larva
        if larva.model.id == id
            larvaindex = i
        end
    end
    larvavisible = larvae.larvae[larvaindex].larva.visible
    on(hovered) do b
        if larvae.hovering_active[]
            if b
                if larvavisible[]
                    larvae.hovered_larva[] = larvaindex
                end
            else
                i = larvae.hovered_larva[]
                if i != 0
                    # TODO: additionally condition the below warning on the type of larva
                    #       view (in AssayPlot, warn; in TrackPlot, do not warn)
                    #i == larvaindex || @warn "Larva #$(larvae.larvae[i].larva.model.id) unexpectedly decorated"
                    larvae.hovered_larva[] = 0
                end
            end
        end
    end
    on(clicked) do b
        @assert b
        activatelarva!(controller, id)
        clicked.val = false
    end
    on(edited) do b
        if b
            #@assert reviewed[]
            reviewed[] || @warn "Larva $id not reviewed"
        else
            @info "Discarding changes for larva $id"
            filterevents(controller).discard_larva_edits[] = id
        end
    end
    # on(reviewed) do b
    #     if !b && edited[]
    #         @info "[design] shall we discard the changes?"
    #         edited[] = false
    #     end
    # end
    included = Observable(false)
    LarvaInfo(controller,
              id,
              hovered,
              clicked,
              reviewed,
              edited,
              included,
              dom_id())
end

Bonito.jsrender(session::Session, li::LarvaInfo) = lowerdom(session, li)

function lowerdom(li::LarvaInfo)
    label = "#$(li.id)"
    label = DOM.label(label,
                      onmouseenter=js"()=>{ $(li.hovered).notify(true); }",
                      onmouseleave=js"()=>{ $(li.hovered).notify(false); }",
                      onmouseup=js"()=>{ $(li.clicked).notify(true); }")
    toggle_reviewed = js"(evt)=>{ $(li.reviewed).notify(evt.srcElement.checked); }"
    reviewed_checkbox = DOM.input(type="checkbox",
                                  checked=li.reviewed,
                                  onchange=toggle_reviewed)
    discard_larva_edits = js"""(evt)=>{
        LarvaTagger.discardLarvaEdits(evt.srcElement, $(li.edited), $label);
    }"""
    edited_checkbox = DOM.input(type="checkbox",
                                checked=li.edited,
                                # onchange is not triggered from Julia
                                onchange=discard_larva_edits)
    include = js"(evt)=>{ LarvaTagger.includeLarva(evt.srcElement, $(li.included)); }"
    included_checkbox = DOM.input(type="checkbox",
                                  checked=li.included,
                                  class="included",
                                  onchange=include)
    DOM.tr(DOM.td(label),
           DOM.td(reviewed_checkbox, style="text-align: center;"),
           DOM.td(edited_checkbox, style="text-align: center;"),
           DOM.td(included_checkbox, style="text-align: center;"))
end

struct LarvaFilter
    controller
    entries::Vector{LarvaInfo}
    includeall::AbstractObservable{Bool}
    saveas::AbstractObservable{Bool}
    attributes
    dom_id
end

function LarvaFilter(controller, entries::Vector{LarvaInfo},
        includeall::AbstractObservable{Bool}, saveas::AbstractObservable{Bool}, attributes)
    LarvaFilter(controller, entries, includeall, saveas, attributes, dom_id())
end

function larvafilter(controller::ControllerHub; kwargs...)
    larvafilter(getlarvae(controller), controller; kwargs...)
end

function larvafilter(larvae, controller::ControllerHub; kwargs...)
    entries = [larvainfo(controller, id) for id in sort(collect(keys(larvae)))]
    on(getactivelarva(controller)) do id
        if !isnothing(id)
            for entry in entries
                if id == entry.id
                    entry.reviewed[] = true
                    break
                end
            end
        end
    end
    includeall = Observable(false)
    on(includeall) do b
        for entry in entries
            if entry.included[] != b
                entry.included[] = b
            end
        end
    end
    saveas = Observable(false)
    gethub(controller)[:larvafilter] = LarvaFilter(controller, entries, includeall, saveas, kwargs)
end

getlarvafilter(controller) = gethub(controller)[:larvafilter]


function lowerdom(lf::LarvaFilter)
    outputfilename = getoutputfile(lf.controller).name
    disabled = map(!, lf.saveas)
    setoutputfilename = js"()=>{ LarvaTagger.setOutputFilename($outputfilename); }"
    button = DOM.button("Save as...";
                        type="button",
                        id="saveas",
                        onclick=setoutputfilename,
                        disabled=disabled,
                        style="width: 100%",
                        class=css_button)
    includeall = js"""(evt)=>{
        LarvaTagger.includeAllLarvae(evt.srcElement, $(lf.includeall), $button);
    }"""
    table = scrollable(DOM.tr(DOM.th(""),
                              DOM.th("Reviewed"),
                              DOM.th("Edited"),
                              DOM.th("Export")),
                       DOM.tr(DOM.td("All"),
                              DOM.td(),
                              DOM.td(),
                              DOM.td(DOM.input(type="checkbox",
                                               #checked=false,
                                               id="includeall",
                                               onchange=includeall),
                                     style="text-align: center;")),
                       lowerdom.(lf.entries)...)
    return DOM.div(table,
                   button;
                   with_attributes(lf.attributes; class="panel")...)
end

function Bonito.jsrender(session::Session, lf::LarvaFilter)
    node = lowerdom(session, lf)
    Bonito.onload(session, node, js"""(parent)=>{
        LarvaTagger.uncheckAllCheckboxes(parent.firstElementChild);
    }""")
    return node
end

mutable struct TagController
    hub::ControllerHub
    view
    fallback_color::String
    # deprecation: manualtag is now stored in `hub` as well with key :manualtag
    manualtag
end

function TagController(controller::ControllerHub;
        fallback_color::Union{Nothing, Symbol, String}=nothing,
        manualtag::Union{Nothing, Symbol, String}=nothing,
    )
    taglut = gettags(controller)
    controller[:taghooks] = taghooks = newcontroller(taglut[], taglut)
    for tag in taghooks
        setevents!(controller, tag)
    end
    #
    fallback_color = @something fallback_color theme[:LarvaPlot][:fallback_color]
    TagController(controller,
                  nothing,
                  html_color(fallback_color),
                  manualtag)
end

#getmanualtag(c::TagController) = c.manualtag

newtagname(::TagController) = NoTag

isfrozen(c::TagController, tag) = isfrozen(lookup(gettags(c)[], tag, true))

struct IndividualTagView
    controller::TagController
    tag::ObservableTag # view
    frozen::Bool
    dom_id
end

function IndividualTagView(controller::TagController, tag::ObservableTag, frozen::Bool)
    IndividualTagView(controller, tag, frozen, dom_id())
end

function tagview(controller::TagController, tag::ObservableTag)
    IndividualTagView(controller, tag, isfrozen(tag))
end

isfrozen(tag::IndividualTagView) = tag.frozen

function lowerdom(ti::IndividualTagView)
    tag = ti.tag
    active = tag.active
    disabled = map(!, active)
    name = tag.name
    color = tag.color
    #
    label = DOM.input(type="text",
                      value=name[],
                      onchange=js"(evt)=>{ $name.notify(evt.srcElement.value); }",
                      disabled=disabled,
                      class="tag-name")
    colorpicker = DOM.input(type="color",
                            value=color[],
                            onchange=js"(evt)=>{ $color.notify(evt.srcElement.value); }",
                            disabled=disabled,
                            class="h-8 w-8 rounded shadow tag-color")
    checkbox = DOM.input(type="checkbox",
                         checked=active,
                         onchange=js"(evt)=>{ $active.notify(evt.srcElement.checked); }",
                         class="tag-active")
    return DOM.tr(DOM.td(checkbox),
                  DOM.td(label),
                  DOM.td(colorpicker))
end
function Bonito.jsrender(session::Session, ti::IndividualTagView)
    node = lowerdom(session, ti)
    tag = ti.tag
    selector = dom_selector(ti) * " .tag-name"
    events = tagevents(ti.controller)
    on(session, events.renamefailed) do (name, failure)
        # tag.name[] == failure || return # if event triggered by model failure
        tag.name[] == name || return # if event triggered by view failure
        @info "Reverting to \"$(name)\" after failure: \"$(failure)\""
        evaljs(session, js"document.querySelector($selector).value = $name;")
    end
    notify(tag.active)
    return node
end

function addtag! end

function addtagbutton(controller)
    button = NyxWidgets.Button(" + ";
                               title="New tag",
                               class=css_button,
                               style="width: 100%; height: 2rem; padding: 0px")
    on(button) do _
        addtag!(controller)
    end
    return button
end

struct TagFilter
    controller::TagController
    views::Vector{IndividualTagView}
    addtagbutton::NyxWidgets.Button
    attributes
    dom_id
end

function TagFilter(controller::TagController; kwargs...)
    TagFilter(controller,
              IndividualTagView[],
              addtagbutton(controller),
              kwargs,
              dom_id())
end

function tagfilter(controller; manualtag=nothing)
    gethub(controller)[:tag] = tc = TagController(controller; manualtag=manualtag)
    tc.view = TagFilter(tc)
    return tc.view
end

function lowerdom(tf::TagFilter)
    manualtag = getmanualtag(tf.controller)
    tags = newview!(gethub(tf.controller)[:taghooks])
    for tag in reverse(tags)
        tag.name[] == manualtag && continue
        push!(tf.views, tagview(tf.controller, tag))
    end
    return DOM.div(scrollable(tf.views...),
                   tf.addtagbutton;
                   with_attributes(tf.attributes; class="panel")...)
end
function Bonito.jsrender(session::Session, tf::TagFilter)
    setsession!(tf.controller, session)
    return lowerdom(session, tf)
end

function addtag!(parent::TagController)
    taglut = gettags(parent)
    newtag = ObservableTag(newtagname(parent), parent.fallback_color)
    pushfirst!(taglut[], newtag)
    #
    taghooks = gethub(parent)[:taghooks]
    tag = newcontroller(newtag, taglut)
    pushfirst!(taghooks, tag)
    setevents!(parent, tag)
    #
    # model ordering: highest priority is first, lowest is last
    view = parent.view
    childview = tagview(parent, newview!(tag))
    addtag!(view, childview)
    notify(taglut)
end

function addtag!(view::TagFilter, newtag::IndividualTagView)
    session = getsession(view.controller)
    tag = newtag.tag
    id0 = dom_id(session)
    @assert id0 isa Int
    tagactive = dom_selector(id0+3)
    tagname = dom_selector(id0+5)
    tagcolor = dom_selector(id0+7)
    jsdom = r(session, newtag)
    htmldom = "<table><tbody>" * replace(string(jsdom), "'" => "\\'") * "</tbody></table>"
    domtable = dom_selector(view)
    evaljs(session, js"""
    let domtable = document.querySelector($domtable).firstElementChild;
    LarvaTagger.insertNewTag(domtable, $htmldom);
    document.querySelector($tagactive).addEventListener('change', (evt)=>{ $(tag.active).notify(evt.srcElement.checked); });
    document.querySelector($tagname).addEventListener('change', (evt)=>{ $(tag.name).notify(evt.srcElement.value); });
    document.querySelector($tagcolor).addEventListener('change', (evt)=>{ $(tag.color).notify(evt.srcElement.value); });
    """)
    push!(view.views, newtag)
    return jsdom
end

struct TagSelector
    controller::TagController
    tagnames::Observable{Vector{String}}
    selected::Observable{Vector{Tuple{String, Observable{Bool}}}}
    selectable::Bool
    showtoggle::Bool
    fromjs::Union{Nothing, Observable{String}}
    dom_id
end

function TagSelector(controller::TagController,
        selectable::Bool=true,
        multiple::Union{Nothing, Bool}=nothing,
    )
    tag_lut = gettags(controller)
    manualtag = getmanualtag(controller)
    tagnames = map(tag_lut) do tags
        [tag.name[] for tag in tags if tag.active[] && tag.name[] != manualtag]
    end
    registered_observables = Observable{Bool}[]
    selectedtags = map(tagnames; ignore_equal_values=true) do names
        for i in length(registered_observables)+1:length(names)
            newobs = Observable(false)
            on(newobs) do b
                @debug "Tag $(i) $(b ? "" : "un")activated"
            end
            push!(registered_observables, newobs)
        end
        collect(zip(names, registered_observables[1:length(names)]))
    end
    refresh_view = map(selectedtags) do _
        true
    end
    multitag = filterevents(controller).allow_multiple_tags
    if !isnothing(multiple)
        multitag[] = multiple
    end
    fromjs = nothing
    if selectable
        fromjs = Observable("")
        on(fromjs) do actuatedtag
            for (tag, selected) in selectedtags[]
                if tag == actuatedtag
                    if isfrozen(controller, tag)
                        @warn "Actuated tag is frozen" actuatedtag
                        selected[] = !selected[] # this observable reflects the state of the
                        # UI; the option element is in an improper state, which will be
                        # undone on the next update of the selector
                    else
                        toggletag!(controller, tag, selected, selectedtags, selectable)
                        flag_active_larva_as_edited(controller)
                    end
                    break
                end
            end
        end
    end
    activelarva = getactivelarva(controller)
    onany(gettimestep(controller),
          refresh_view,
          tag_lut,
          multitag,
         ) do timestep, _, _, multitag
        id = activelarva[]
        isnothing(id) && return
        if isempty(selectedtags[]) && any(tag -> tag.active[], tag_lut[])
            # Workaround: generate view if not done yet.
            # Note that the view is generated anyway, but empty,
            # which is fine when no tags are defined.
            # Notifying `tag_lut` actually triggers a bug if
            # empty, e.g. on loading Chore files.
            # This also triggers another bug if all tags are unchecked.
            notify(tag_lut)
            return
        end
        if multitag
            refresh_selected_tags(controller, id, timestep, tag_lut, selectedtags, selectable)
        else
            refresh_selected_tag(controller, id, timestep, tag_lut, selectedtags, selectable)
        end
    end
    on(multitag) do b
        @debug (b ? "Multiple tags" : "Single tag") * " per timestep"
    end
    on(filterevents(controller).discard_larva_edits) do id
        activelarva[] == id || return
        #notify(refresh_view) # applies only to the tag selector
        notify(gettimestep(controller)) # full refresh
    end
    return TagSelector(controller,
                       tagnames,
                       selectedtags,
                       selectable,
                       isnothing(multiple),
                       fromjs,
                       dom_id())
end

function refresh_selected_tags(controller, id, timestep, taglut, selectedtags, selectable)
    larva = getlarva(controller, id)
    tags = getusertags(larva, timestep; lut=taglut)
    refresh_selected_tags(controller, tags, selectedtags, selectable)
end

function refresh_selected_tags(controller, tags::UserTags, selectedtags, selectable)
    tagnames = [tag.name[] for tag in tags]
    jscode = String[]
    for (name, selected) in Observables.to_value(selectedtags)
        isempty(name) && continue
        selected′ = name in tagnames
        if !selectable || selected[] != selected′
            selected[] = selected′
            push!(jscode, "document.getElementById('$(name)').selected = $(selected′);")
        end
    end
    isempty(jscode) && return
    evaljs(getsession(controller),
           Bonito.JSCode([Bonito.JSString(join(jscode, "\n"))]))
end

function refresh_selected_tag(controller, id, timestep, taglut, selectedtags, selectable)
    larva = getlarva(controller, id)
    tags = getusertags(larva, timestep; lut=taglut)
    refresh_selected_tag(controller, tags, selectedtags, selectable)
end

function refresh_selected_tag(controller, tags::UserTags, selectedtags, selectable)
    for tag in tags
        if tag.active[]
            return refresh_selected_tag(controller, tag, selectedtags, selectable)
        end
    end
    refresh_selected_tag(controller, nothing, selectedtags, selectable)
end

function refresh_selected_tag(controller,
        tag::Union{Nothing, ObservableTag},
        selectedtags,
        selectable,
    )
    jscode = String[]
    for (name, selected) in Observables.to_value(selectedtags)
        isempty(name) && continue
        selected′ = !isnothing(tag) && name == tag.name[]
        if !selectable || selected[] != selected′
            selected[] = selected′
            push!(jscode, "document.getElementById('$(name)').selected = $(selected′);")
        end
    end
    isempty(jscode) && return
    evaljs(getsession(controller),
           Bonito.JSCode([Bonito.JSString(join(jscode, "\n"))]))
end

function toggletag!(controller, tagname, selected, selectedtags, selectable)
    multitag = filterevents(controller).allow_multiple_tags[]
    larva_id = getactivelarva(controller)[]
    larva = getlarva(controller, larva_id)
    timestep = gettimestep(controller)[]
    taglut = gettags(controller)[] # should not be mutated here, therefore we use the model
    # instead of the view; otherwise use the view gethub(controller)[:taghooks]
    tags = getusertags!(larva, timestep; lut=taglut)
    tag = lookup(taglut, tagname)
    isnothing(tag) && throw(KeyError(tagname))
    # toggle
    selected′= !selected[]
    transaction = if selected′
        if multitag || isempty(tags)
            settag!(tags, tag)
            refresh_selected_tags(controller, tags, selectedtags, selectable)
            AddTag(larva_id, timestep, tagname)
        else
            settag!(tags, tag; single=true)
            refresh_selected_tag(controller, tag, selectedtags, selectable)
            OverrideTags(larva_id, timestep, tagname, collect(tags))
        end
    else
        @assert !isempty(tags)
        deletetag!(tags, tag)
        RemoveTag(larva_id, timestep, tagname)
    end
    push!(transactions(controller), transaction)
    notify(larva.usertags)
end

function lowertags(selectedtags, selectable)
    toggle = selectable ? js"LarvaTagger.toggleTagAtPointer" : nothing
    DOM.select(DOM.option(tagname;
                          id=tagname,
                          value=tagname,
                          selected=selected[],
                          onmousedown=toggle)
               for (tagname, selected) in selectedtags;
               multiple=true,
               size=1)
end

function lowerdom(ts::TagSelector)
    taglist = DOM.div(lowertags(ts.selected[], ts.selectable); id="tag-selector")
    elements = [taglist]
    if ts.showtoggle
        multitag = filterevents(ts.controller).allow_multiple_tags
        toggle_multitag = js"(evt)=>{ $multitag.notify(evt.srcElement.checked); }"
        checkbox = DOM.div(DOM.input(type="checkbox",
                                     checked=multitag,
                                     onchange=toggle_multitag,
                                     class="m-1"),
                           DOM.label("Multiple tags per time step");
                           class="flex flex-row")
        push!(elements, checkbox)
    end
    return DOM.div(elements...;
                   class="flex flex-col",
                   onmouseenter=js"LarvaTagger.focusOnTimeSlider")
end
function Bonito.jsrender(session::Session, ts::TagSelector)
    setsession!(ts.controller, session)
    node = lowerdom(session, ts)
    if ts.selectable
        evaljs(session, js"LarvaTagger.setTagSelector($(ts.fromjs))")
    end
    on(session, ts.selected) do selected
        jsdom = r(session, lowertags(selected, ts.selectable))
        htmldom = replace(string(jsdom), "'" => "\\'")
        evaljs(session, js"""
        document.getElementById('tag-selector').innerHTML = $htmldom;
        let tags = document.querySelectorAll('#tag-selector option');
        for (let i = 0; i < tags.length; i++) {
            tags[i].addEventListener('mousedown', LarvaTagger.toggleTagAtPointer);
        }
        """)
    end
    return node
end

function tagselector(controller;
        editabletags::Bool=true,
        multipletags::Union{Nothing, Bool}=nothing,
    )
    c = gethub(controller)
    controller = haskey(c, :tag) ? c[:tag] : TagController(c)
    TagSelector(controller, editabletags, multipletags)
end

mutable struct TrackViewer
    plot::TrackPlot
    player::PlayerView
    tagselector::TagSelector
    dom_id
end

function trackviewer(controller;
        editabletags::Bool=true,
        multipletags::Union{Nothing, Bool}=nothing,
        kwargs...
    )
    model = getlarvae(controller)
    isempty(model) && return nothing
    plot = trackplot(model, controller; editabletags=editabletags, kwargs...)
    play = player(controller)
    sele = tagselector(controller; editabletags=editabletags, multipletags=multipletags)
    TrackViewer(plot, play, sele, dom_id())
end

function lowerdom(tv::TrackViewer)
    focus = js"(evt)=>{ LarvaTagger.focusOnTimeSlider(evt.srcElement); }"
    return DOM.div(tv.plot,
                   tv.player,
                   tv.tagselector;
                   class="flex flex-col",
                   style="display: none;",
                   id="trackviewer",
                   onmouseenter=focus)
end
Bonito.jsrender(session::Session, tv::TrackViewer) = lowerdom(session, tv)

struct MetadataEditor
    controller
    max_entries::Int
    dom_id
end

MetadataEditor(controller) = MetadataEditor(controller, 10, dom_id())

metadataeditor(controller) = MetadataEditor(controller)

function lowerdom(me::MetadataEditor)
    table = getmetadatatable(me.controller, me.max_entries)
    return DOM.div(DOM.div(DOM.input(value=fieldname,
                                     type="text",
                                     class="headerfield",
                                     onchange=js"""(evt)=>{
                                        $fieldname.notify(evt.srcElement.value);
                                     }"""),
                           DOM.input(value=fieldvalue,
                                     type="text",
                                     class="valuefield",
                                     onchange=js"""(evt)=>{
                                        $fieldvalue.notify(evt.srcElement.value);
                                     }""");
                           class="flex flex-row")
                   for (fieldname, fieldvalue) in pairs(table);
                   id="metadata-panel",
                   class="flex flex-col panel")
end
Bonito.jsrender(session::Session, me::MetadataEditor) = lowerdom(session, me)

struct FileMenu
    controller::ControllerHub
    browser::FileBrowser
end

function filemenu(controller; upload_button=false, download_button=false,
        prepare_download=nothing, create_directory_button=false, delete_button=false,
        kwargs...)
    wd = getworkingdir(controller)
    dir = joinpath(wd.root, wd.path[])
    browser = FileBrowser(dir; root=wd.root, upload_button=upload_button,
                          download_button=download_button,
                          prepare_download=prepare_download,
                          create_directory_button=create_directory_button,
                          delete_button=delete_button)
    gethub(controller)[:browser] = browser.model
    on(FileBrowsers.selectedfile(browser)) do file
        tryopenfile(controller, file; reload=true)
    end
    workingdir = FileBrowsers.workingdir(browser)
    on(workingdir) do _
        dir = workingdir[]
        # maintain the deprecated WorkingDirectory as it may still be in use elsewhere
        wd.path[] = dir
        wd.content[] = [d for (_, d) in browser.model.content[]]
    end
    if FileBrowsers.supports_upload(browser)
        on(FilePickers.uploadedfile(browser)) do fileinfo
            filepath = joinpath(workingdir[], fileinfo["name"])
            saveinputfile(filepath, fileinfo["content"])
            notify(workingdir)
        end
    end
    return FileMenu(controller, browser)
end

function Bonito.jsrender(session::Session, fm::FileMenu)
    r(session, DOM.div(fm.browser; class="panel"))
end

function globalrefresh(::Type{Session}, controller)
    @info "Refreshing the view"
    evaljs(getsession(controller), js"LarvaTagger.reloadPage()")
    #turn_load_animation_off(controller)
end

function setedited!(c::LarvaFilter, id)
    for entry in c.entries
        if entry.id == id
            entry.edited[] = true
            break
        end
    end
end

function addtosavequeue!(c::LarvaFilter, id)
    for entry in c.entries
        if entry.id == id
            entry.included[] = true
            break
        end
    end
    c.saveas[] = true
end

struct BackendMenu
    backends::Backends
    send2backend::NyxWidgets.Button
    dom_id
end

function backendmenu(controller, location)
    backends = getbackends(controller, location)
    disabled = map(isempty, backends.model_instances)
    send2backend = NyxWidgets.Button("Autotag"; disabled=disabled)
    on(send2backend) do _
        predict(backends, controller[:input][])
    end
    BackendMenu(backends, send2backend, dom_id())
end

function lowerdom(bs::BackendMenu)
    models = bs.backends.model_instances
    model_instance = bs.backends.model_instance
    update_model_instance = js"(evt)=>{ $model_instance.notify(evt.srcElement.value); }"
    select_dom = DOM.select(DOM.option(model; value=model) for model in models[];
                            onchange=update_model_instance,
                            class=css_button)
    active_backend = bs.backends.active_backend
    update_active_backend = js"(evt)=>{ $active_backend.notify(evt.srcElement.value); }"
    return DOM.div(DOM.label("Backend"),
                   DOM.select(DOM.option(backend; value=backend)
                              for backend in bs.backends.backends;
                              onchange=update_active_backend,
                              class=css_button),
                   DOM.label("Model instance"),
                   select_dom,
                   bs.send2backend;
                   class="flex flex-col panel")
end
function Bonito.jsrender(session::Session, bs::BackendMenu)
    node = lowerdom(session, bs)
    onjs(session, bs.backends.model_instances, js"""(options) => {
         select = document.querySelectorAll($(dom_selector(bs)) + ' > select')[1];
         LarvaTagger.updateSelectOptions(select, options);
    }""")
    return node
end

struct LoadAnimation
    visible::Observable{Bool}
    dom_id
end

LoadAnimation() = LoadAnimation(Observable(true), dom_id())

function loadanimation(controller)
    la = LoadAnimation()
    gethub(controller)[:loadanimation] = la.visible
    return la
end

function lowerdom(::LoadAnimation)
    DOM.div(DOM.div(); id="load-animation", class="la-ball-clip-rotate")
end
function Bonito.jsrender(session::Session, la::LoadAnimation)
    node = lowerdom(session, la)
    onjs(session, la.visible, js"(b)=>{ $node.style.display = (b ? 'block' : 'none'); }")
    return node
end

struct TwoOptionDialog
    answer::Observable{Bool}
    title::Observable{String}
    prompt::Observable{String}
    button1::Observable{String}
    button2::Observable{String}
    visible::Observable{Bool}
    attributes
    dom_id
end

TwoOptionDialog(; kwargs...) = TwoOptionDialog(Observable(true),
                                               Observable(""),
                                               Observable(""),
                                               Observable(""),
                                               Observable(""),
                                               Observable(false),
                                               with_attributes(kwargs; style="display:none"),
                                               dom_id())

twooptiondialog(controller) = get!(gethub(controller), :twooptiondialog, TwoOptionDialog())

function twooptiondialog(controller, answer, title, message, button1, button2)
    tod = twooptiondialog(controller)
    tod.title[] = title
    tod.prompt[] = message
    tod.button1[] = button1
    tod.button2[] = button2
    evaljs(getsession(controller), js"""
    document.getElementById('two-option-dialog-title').innerText = $title;
    document.getElementById('two-option-dialog-prompt').innerText = $message;
    document.getElementById('two-option-dialog-button1').innerHTML = $button1;
    document.getElementById('two-option-dialog-button2').innerHTML = $button2;
    """)
    tod.visible[] = true
    Observables.clear(tod.answer)
    on(tod.answer) do ans
        tod.visible[] = false
        answer[] = ans
    end
end

function lowerdom(tod::TwoOptionDialog)
    DOM.div(DOM.div(tod.title[]; id="two-option-dialog-title"),
            DOM.div(tod.prompt[]; id="two-option-dialog-prompt"),
            DOM.div(
                    DOM.button(tod.button1[];
                               type="button",
                               id="two-option-dialog-button1",
                               class="button",
                               onclick=js"()=>{ $(tod.answer).notify(true); }"),
                    DOM.button(tod.button2[];
                               type="button",
                               id="two-option-dialog-button2",
                               class="button",
                               onclick=js"()=>{ $(tod.answer).notify(false); }");
                    id="two-option-dialog-buttons");
            id="two-option-dialog",
            tod.attributes...)
end
function Bonito.jsrender(session::Session, tod::TwoOptionDialog)
    node = lowerdom(session, tod)
    onjs(session, tod.visible, js"(b)=>{ $node.style.display = (b ? 'block' : 'none'); }")
    return node
end

