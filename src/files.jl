
# deprecated; may be replaced by FileBrowsers.Model.FileBrowser
struct WorkingDirectory
    controller
    root::String # could be hidden (not stored)
    path::Validator{String}
    content::Observable{Vector{String}}
end

function WorkingDirectory(controller, root::String, path::String="")
    WorkingDirectory(controller, root, Validator(path), Observable(String[]))
end

function workingdir(controller, root::String, path::String=""; secure::Bool=true)
    if secure
        root = realpath(root)
    end
    isdir(root) || throw(ArgumentError("cannot find root directory"))
    if isempty(path)
        path = "."
    elseif isabspath(path)
        path = relpath(path, root)
        startswith(path, "..") && throw(ArgumentError("path not below root: $path"))
    end
    wd = WorkingDirectory(controller, root, path)
    # on(wd.path, Val(1)) do newpath
    #     try
    #         if isempty(newpath)
    #             newpath = "."
    #         elseif isabspath(newpath)
    #             fullpath = newpath
    #             newpath = relpath(newpath, root)
    #         end
    #         fullpath = joinpath(root, newpath)
    #         if secure
    #             fullpath = realpath(fullpath)
    #         end
    #         if startswith(relpath(fullpath, root), "..")
    #             @logmsg SecurityAlert "Path outside of chroot" path=newpath client=identifyclient(controller)
    #             throw(Exception)
    #         elseif !isdir(fullpath)
    #             @debug "Path is not a directory" path=newpath
    #             tryopenfile(controller, fullpath; reload=true)
    #             throw(Exception)
    #         end
    #         return Validate(newpath)
    #     catch
    #         return Invalidate()
    #     end
    # end
    # on(wd.path.model) do dir
    #     @assert !isempty(dir)
    #     content = [entry for entry in readdir(joinpath(root, dir))
    #                if entry[1] != '.']
    #     if dir != "."
    #         pushfirst!(content, "..")
    #     end
    #     wd.content[] = content
    # end
    return wd
end

function getworkingdir(controller)
    hub = gethub(controller)
    wd = nothing
    try
        wd = hub[:workingdirectory]
    catch
        hub[:workingdirectory] = wd = workingdir(hub, pwd())
    end
    return wd
end

getpath(wd::WorkingDirectory) = getview!(wd.path)
getpath(controller) = getpath(getworkingdir(controller))

function cwd(wd, subdir::String="")
    path = getpath(wd)
    if !isempty(subdir)
        path[] = normpath(joinpath(path[], subdir))
    end
    return path[]
end

getwdcontent(wd::WorkingDirectory) = wd.content
getwdcontent(controller) = getwdcontent(getworkingdir(controller))


# open/save

function tryopenfile(controller, path; reload::Bool=false)
    hub = gethub(controller)
    refresh_view = reload
    if path isa Ref
        input, path = path, path[]
        if haskey(hub, :input)
            @assert hub[:input] === input
        else
            hub[:input] = input
        end
        isnothing(path) && return
    end
    if refresh_view
        turn_load_animation_on(controller)
    end
    #
    records = loadfile(path)
    if isempty(records)
        @info "Cannot load file" file=path
        return
    elseif haskey(hub, :input)
        hub[:input][] = path
    end
    # tag_lut
    fallback_color = theme[:LarvaPlot][:fallback_color]
    tag_lut = ObservableTag[]
    if hasproperty(records, :tagcolors)
        existingtags = records.tags
        existingcolors = records.tagcolors
        for (tag, color) in zip(existingtags, existingcolors)
            original = TagModel(tag, color, true)
            push!(tag_lut, ObservableTag(original; color=color, active=true))
        end
    elseif hasproperty(records, :tags)
        # trx.mat tags
        default_convention = Dict(
                                  :back_large => :cyan,
                                  :cast_large => :red,
                                  :hunch_large => :blue,
                                  :roll_large => :yellow,
                                  :run_large => :black,
                                  :small_motion => :grey50,
                                  :stop_large => :green,
                                 )
        related_tags = Dict(
                            :back => :back_large,
                            :back_weak => :back_large,
                            :back_strong => :back_large,
                            :cast => :cast_large,
                            :cast_weak => :cast_large,
                            :cast_strong => :cast_large,
                            :hunch => :hunch_large,
                            :hunch_weak => :hunch_large,
                            :hunch_strong => :hunch_large,
                            :roll => :roll_large,
                            :roll_weak => :roll_large,
                            :roll_strong => :roll_large,
                            :run => :run_large,
                            :run_weak => :run_large,
                            :run_strong => :run_large,
                            :stop => :stop_large,
                            :stop_weak => :stop_large,
                            :stop_strong => :stop_large,
                            :bend => :cast_large,
                            :crawl => :run_large,
                            :BACK => :back_large,
                            :BEND => :cast_large,
                            :HUNCH => :hunch_large,
                            :ROLL => :roll_large,
                            :RUN => :run_large,
                            :STOP => :stop_large,
                            Symbol("back-up") => :back_large,
                            Symbol("small action") => :small_motion,
                           )
        existingtags = records.tags
        # following issue #55, we need to identify situations where _large-suffixed
        # tags are found together with their suffix-free variants
        apply_default_convention = false
        for tag in existingtags
            if tag in keys(default_convention)
                variant, suffix = rsplit(string(tag), '_'; limit=2)
                if suffix == "large" && Symbol(variant) in existingtags
                    apply_default_convention = true
                    break
                end
            end
        end
        #
        for tag in existingtags
            active′= tag in keys(default_convention)
            tag′= active′ ? tag : get(related_tags, tag, tag)
            color = get(default_convention, tag′, fallback_color)
            original = TagModel(tag)
            active = !apply_default_convention || active′
            push!(tag_lut, ObservableTag(original; color=color, active=active))
        end
    end
    secondarytags = records[:secondarytags]
    if !isnothing(secondarytags)
        manualtag = getmanualtag(hub)
        for tag in secondarytags
            original = TagModel(tag, true; frozen=tag == manualtag)
            push!(tag_lut, ObservableTag(original; color=fallback_color, active=true))
        end
    end
    #
    hub[:output] = records.output
    getpath(hub)[] = dirname(path)
    newcontroller(hub, :larva, LarvaController,
                  records.tracks, tag_lut, records.timestamps)
    if refresh_view
        globalrefresh(hub[:frontend], hub)
    end
end

function loadfile(path)
    file = Formats.load(path)
    data = isempty(file.run) ? file.timeseries : file.run
    # tracks
    if file isa Formats.FIMTrack
        times = PlanarLarvae.times(data)
        tracks = [LarvaModel(track, times) for track in values(getrun(file))]
    elseif file isa Formats.MaggotUBA
        times = PlanarLarvae.times(data)
        tracks = Formats.astimeseries(getrun(file); labels2tags=true)
        tracks = [LarvaModel(id, ts, times) for (id, ts) in tracks]
    else
        times = PlanarLarvae.times(gettimeseries(file))
        tracks = [LarvaModel(id, ts, times) for (id, ts) in pairs(gettimeseries(file))]
    end
    # dataset
    metadata = getmetadata(file)
    if isempty(metadata)
        metadata = extract_metadata_from_filepath(path)
        run = get!(metadata, :date_time, "NA")
    else
        run = file.run.id
    end
    output = Dataset([Run(run; metadata...)])
    #
    labels = getlabels(file)
    existingtags = labels[:names]
    secondarytags = file isa Formats.JSONLabels ? getsecondarylabels(file) : nothing
    if haskey(labels, :colors)
        tagcolors = labels[:colors]
        return (tracks=tracks, timestamps=times, tags=existingtags, tagcolors=tagcolors,
                secondarytags=secondarytags, output=output)
    else
        return (tracks=tracks, timestamps=times, tags=existingtags,
                secondarytags=secondarytags, output=output)
    end
end

getoutput(controller) = gethub(controller)[:output]

interpolate(s="yyyymmdd_HHMMSS") = Dates.format(Dates.now(), s)

function savetofile(controller, file; datafile=nothing, merge=false)
    dir = cwd(controller)
    if '{' in file
        prefix, remainder = split(file, '{'; limit=2)
        if '}' in remainder
            parts = split(remainder, '}')
            if length(parts) == 2
                infix, suffix = parts
            else
                @warn "Too many braces" file
                infix, suffix = join(parts[1:end-1], '}'), parts[end]
            end
            file = prefix * interpolate(infix) * suffix
        end
    end
    filepath = joinpath(dir, file)
    if merge
        tmpfile = tempname(dir; cleanup=false) * ".label"
        tmpfile′= basename(tmpfile)
        try
            savetofile(controller, tmpfile′; datafile=datafile)
            @info "Merging file $tmpfile′ into $(file)"
            merge′(filepath, tmpfile′, getmanualtag(controller), file)
        finally
            rm(tmpfile; force=true)
        end
        return
    end
    dataset = getoutput(controller)
    @assert length(dataset) == 1
    run = first(values(dataset))
    lut = gettags(controller)[]
    manualtag = getmanualtag(controller)
    has_explicit_editions = false
    if !explicit_editions_needed(controller, manualtag)
        manualtag = nothing
    end
    larvafilter = getlarvafilter(controller)
    empty = true
    for larvainfo in sort(larvafilter.entries; by=entry->entry.id)
        if larvainfo.included[]
            id = larvainfo.id
            larva = getlarva(controller, id)
            timeseries = [t for (t, _) in larva.fullstates]
            track = Track(id, timeseries)
            labels = Union{String, Vector{String}}[]
            for timestep in larva.alignedsteps
                tags, differ = differentialtags(larva, timestep; lut=lut, difftag=manualtag)
                push!(labels, length(tags) == 1 ? tags[1] : tags)
                if differ
                    has_explicit_editions = true
                end
            end
            track[:labels] = labels
            run[id] = track
            empty = false
        end
    end
    metadata = run.attributes[:metadata]
    mdt = gethub(controller)[:metadatatable][]
    for item in mdt.entries
        name = item.name.model[]
        value = item.value.model[]
        if !(isempty(name) || isempty(value))
            metadata[Symbol(name)] = value
        end
    end
    if empty
        @info "Not any larva included"
    else
        @info "Saving to file" file=filepath
        if has_explicit_editions
            secondarylabels = get!(dataset.attributes, :secondarylabels, String[])
            if manualtag ∉ secondarylabels
                push!(secondarylabels, manualtag)
            end
        end
        for tag in lut
            if !isnothing(tag.original) && tag.original.secondary
                secondarytag = tag.name[]
                secondarylabels = get!(dataset.attributes, :secondarylabels, String[])
                if secondarytag ∉ secondarylabels
                    push!(secondarylabels, secondarytag)
                end
            end
        end
        #
        dataset = encodelabels(dataset)
        labels = dataset.attributes[:labels]
        if labels isa Vector
            labels = Dict{Symbol, Any}(:names => labels)
            dataset.attributes[:labels] = labels
        end
        colors = [lookup(lut, label, true).color[] for label in labels[:names]]
        labels[:colors] = colors
        if isnothing(datafile)
            datafile = getinputfile(controller)[]
            if isempty(datafile)
                datafile = gethub(controller)[:input][]
            end
        end
        deps = getdependencies(gethub(controller)[:input][])
        if isempty(deps)
            datafilepath = isfile(datafile) ? datafile : joinpath(cwd(controller), datafile)
            foreach(find_associated_files(datafilepath)) do dep
                Datasets.pushdependency!(dataset, dep.source)
            end
        else
            for dep in deps
                Datasets.pushdependency!(dataset, dep)
            end
        end
        Datasets.to_json_file(filepath, dataset)
        Taggers.check_permissions(filepath)
        notify(FileBrowsers.workingdir(gethub(controller)[:browser]))
    end
end

function differentialtags(larva, timestep; lut, difftag=nothing)
    lut = Observables.to_value(lut)
    usertags = larva.usertags[]
    # note the difference between missing user tags (unchanged) and empty user tags that
    # may signal explicitly removed tags, if some tags were originally defined
    recordedtags = if haskey(usertags, timestep)
        tags = usertags[timestep]
        String[tag.name[] for tag in tags if tag.active[]]
    else
        nothing
    end
    #
    originaltags = String[]
    firststep = larva.alignedsteps[1]
    @assert firststep <= timestep
    relstep = timestep - firststep + 1
    @assert relstep ∉ larva.missingsteps
    relstep -= count(larva.missingsteps .< relstep)
    @assert relstep <= length(larva.fullstates)
    _, state = larva.fullstates[relstep]
    if hasproperty(state, :tags)
        tagnames = convert(Vector{String}, state.tags)
        for tagname in tagnames
            tag = lookup(lut, tagname)
            if isnothing(tag)
                @error "Tag \"$tagname\" not found in LUT" lut
            elseif tag.active[]
                push!(originaltags, tag.name[])
            end
        end
    end
    #
    differ = false
    difftags = String[]
    if isnothing(recordedtags)
        difftags = originaltags
    elseif isempty(symdiff(recordedtags, originaltags)) || isnothing(difftag) || difftag ∈ recordedtags
        difftags = recordedtags
    else
        difftags = push!(recordedtags, difftag)
        differ = true
    end
    return difftags, differ
end

function explicit_editions_needed(controller, editiontag)
    originalfile = getinputfile(controller)[]
    if isempty(originalfile)
        originalfile = gethub(controller)[:input][]
    end
    isnothing(originalfile) && return false
    file = preload(originalfile)
    if file isa Formats.JSONLabels
        Formats.load!(file)
        return haskey(file.run.attributes, :labels)
    elseif file isa Formats.Trxmat || file isa Formats.MaggotUBA
        return true
    else
        return false
    end
end

function getinputfile(controller)
    hub = gethub(controller)
    inputfile = nothing
    try
        inputfile = hub[:inputfile]
    catch
        hub[:inputfile] = inputfile = Observable{Union{Nothing, String}}("")
        on(inputfile) do f
            if isnothing(f)
                inputfile.val = ""
            else
                tryopenfile(controller, f; reload=true)
            end
        end
    end
    return inputfile
end

struct OutputFile
    name
    merge
end

OutputFile() = OutputFile(
    Observable{Union{Nothing, String}}("{yyyymmdd_HHMMSS}.label"),
    Observable{Bool}(false),
)

function reset!(outputfile::OutputFile)
    outputfile.name[] = nothing
end

function getoutputfile(controller)
    hub = gethub(controller)
    outputfile = nothing
    try
        outputfile = hub[:outputfile]
    catch
        hub[:outputfile] = outputfile = OutputFile()
        on(outputfile.name) do file
            dir = cwd(controller)
            if isnothing(file)
                outputfile.name.val = "{yyyymmdd_HHMMSS}.label"
            elseif isfile(joinpath(dir, file))
                twooptiondialog(hub, outputfile.merge,
                    "File already exists",
                    "Do you want to save the manual editions only (merge), or entirely overwrite the file?",
                    "Merge", "Overwrite")
            else
                savetofile(hub, file)
                reset!(outputfile)
            end
        end
        on(outputfile.merge) do merge
            if !isnothing(merge)
                savetofile(hub, outputfile.name[]; merge=merge)
                reset!(outputfile)
            end
        end
    end
    return outputfile
end

const valid_inputfile_formats = (".mat", ".spine", ".outline", ".csv", ".label", ".json",
                                 ".labels")

function saveinputfile(path, content)
    parts = splitext(path)
    if length(parts) == 2 && parts[2] in valid_inputfile_formats
        if ispath(path)
            if isfile(path)
                @warn "File already exists" path
            else
                @error "File already exists and is not a regular file" path
                return
            end
        end
        open(path, "w") do f
            write(f, content)
        end
        Taggers.check_permissions(path)
    else
        @error "File format not admitted" fileparts=parts
    end
end
