module Taggers

import PlanarLarvae.Formats, PlanarLarvae.Dataloaders

export Tagger, isbackend, resetmodel, resetdata, train, predict, finetune, embed

struct Tagger
    backend_dir::String
    model_instance::String
    sandbox::Union{Nothing, String}
    output_filenames::Dict{String, String}
end

function Tagger(backend_dir::String, model_instance::String)
    Tagger(backend_dir, model_instance, nothing, Dict{String, String}())
end
Tagger(backend_dir, model_instance) = Tagger(string(backend_dir), string(model_instance))

function isolate(tagger)
    rawdatadir = joinpath(tagger.backend_dir, "data", "raw")
    mkdir(rawdatadir)
    rawdatadir = mktempdir(rawdatadir; cleanup=false)
    Tagger(tagger.backend_dir, tagger.model_instance, basename(rawdatadir),
           tagger.output_filenames)
end

const envdir = get(ENV, "VENV_DIR", "venv")

function isbackend(path)
    bindir = Sys.iswindows() ? "Scripts" : "bin"
    return isdir(joinpath(path, "models")) &&
           isfile(joinpath(path, "pyproject.toml")) &&
           (isfile(joinpath(path, "poetry.lock")) ||
            (isfile(joinpath(path, envdir, bindir, "python")) &&
             isfile(joinpath(path, envdir, bindir, "tagging-backend"))))
end
isbackend(tagger::Tagger) = isbackend(tagger.backend_dir)

function tagging_backend_command(path)
    bindir = Sys.iswindows() ? "Scripts" : "bin"
    python = joinpath(path, envdir, bindir, "python")
    tagging_backend_script = joinpath(path, envdir, bindir, "tagging-backend")
    if isfile(python) && isfile(tagging_backend_script)
        `$python $tagging_backend_script`
    else
        `poetry run tagging-backend`
    end
end
tagging_backend_command(tagger::Tagger) = tagging_backend_command(tagger.backend_dir)

modeldir(tagger::Tagger) = joinpath(tagger.backend_dir, "models", tagger.model_instance)

datadir(tagger::Tagger, stage::String) = joinpath(tagger.backend_dir, "data", stage,
        something(tagger.sandbox, tagger.model_instance))

function reset(tagger::Tagger)
    resetmodel(tagger)
    resetdata(tagger)
end

function reset(dir::String)
    try
        rm(dir; recursive=true)
    catch
    end
    mkpath(dir)
    check_permissions(dir)
end

resetmodel(tagger::Tagger) = reset(modeldir(tagger))

function resetdata(tagger::Tagger)
    for dir in ("raw", "interim", "processed")
        resetdata(tagger, dir)
    end
end

resetdata(tagger::Tagger, dir::String) = reset(datadir(tagger, dir))

function removedata(tagger::Tagger)
    for dir in ("raw", "interim", "processed")
        try
            rm(datadir(tagger, dir); recursive=true)
        catch
        end
    end
end

"""
    push(tagger, filepath)
    push(tagger, dirpath)
    push(tagger, filelistfile)

Copy a data file into the tagger's raw data directory,
or link a data directory as the tagger's raw data directory,
or read relative filepaths from a .txt file and copy all listed files
into the tagger's raw data directory, preserving the directory structure
in the relative paths.
"""
function push(tagger::Tagger, inputdata::String)
    destination = nothing
    backend_name = basename(realpath(tagger.backend_dir))
    raw_data_dir = datadir(tagger, "raw")
    if occursin('*', inputdata)
        repository = Dataloaders.Repository(inputdata)
        for file in Formats.find_associated_files(Dataloaders.files(repository))
            src_file = file.source
            @debug "Pushing file to backend" backend=backend_name instance=tagger.model_instance file=src_file
            dest_file = normpath(joinpath(raw_data_dir, relpath(src_file, repository.root)))
            src_file = normpath(src_file)
            if dest_file != src_file
                dest_dir = dirname(dest_file)
                mkpath(dest_dir)
                open(src_file, "r") do f
                    open(dest_file, "w") do g
                        write(g, read(f))
                    end
                end
            end
        end
    elseif isdir(inputdata)
        inputdata = realpath(inputdata) # strip the end slashes
        rm(raw_data_dir; force=true, recursive=true)
        destination = raw_data_dir
        symlink(inputdata, destination)
    elseif endswith(inputdata, ".txt")
        files_by_dir = Dict{String, Vector{String}}()
        for file in readlines(inputdata)
            parent = dirname(file)
            push!(get!(files_by_dir, parent, String[]), abspath(file))
        end
        for (dir, files) in pairs(files_by_dir)
            for file in Formats.find_associated_files(files)
                file = file.source
                @debug "Pushing file to backend" backend=backend_name instance=tagger.model_instance file=file
                dest_dir = normpath(joinpath(raw_data_dir, dir))
                mkpath(dest_dir)
                dest_file = joinpath(dest_dir, basename(file))
                src_file = normpath(file)
                if dest_file != src_file
                    open(src_file, "r") do f
                        open(dest_file, "w") do g
                            write(g, read(f))
                        end
                    end
                end
            end
        end
    else
        file = abspath(inputdata)
        for file in Formats.find_associated_files(file)
            file = file.source
            @info "Pushing file to backend" backend=backend_name instance=tagger.model_instance file=file
            dest_file = normpath(joinpath(raw_data_dir, basename(file)))
            isnothing(destination) && (destination = dest_file)
            src_file = normpath(file)
            if dest_file != src_file
                open(src_file, "r") do f
                    open(dest_file, "w") do g
                        write(g, read(f))
                    end
                end
            end
        end
    end
    return destination
end

function pull(tagger::Tagger, dest_dir::String)
    proc_data_dir = datadir(tagger, "processed")
    isdir(proc_data_dir) || throw("no processed data directory found")
    # dest_dir can be empty on macOS
    dest_dir = isempty(dest_dir) ? pwd() : realpath(dest_dir) # strip end slash
    dest_files = String[]
    for (parent, _, files) in walkdir(proc_data_dir)
        if !isempty(files)
            parent′= joinpath(dest_dir, relpath(parent, proc_data_dir))
            for file in files
                src_file = normpath(joinpath(parent, file))
                file′= get(tagger.output_filenames, file, file)
                dest_file = normpath(joinpath(parent′, file′))
                if dest_file != src_file
                    open(src_file, "r") do f
                        open(dest_file, "w") do g
                            write(g, read(f))
                        end
                    end
                    check_permissions(dest_file)
                end
                push!(dest_files, dest_file)
            end
        end
    end
    if !isnothing(tagger.sandbox)
        # emulate mktempdir(...; cleanup=true) as if called for all 3 data subdirectories
        removedata(tagger)
    end
    return dest_files
end

"""
    check_permissions(filepath)

Attempt to fix file ownership.
This is useful in a Docker container that runs as root.
Environment variables HOST_UID and HOST_GID are set in the container by the larvatagger.sh script
so that the actual user is known.
"""
function check_permissions(file)
    try
        uid, gid = ENV["HOST_UID"], ENV["HOST_GID"]
        @debug "Changing file ownership" uid gid file
        chown(file, parse(Int, uid), parse(Int, gid))
    catch
    end
end

function parsekwargs!(args, kwargs)
    for (key, value) in pairs(kwargs)
        isnothing(value) && continue
        value === false && continue
        push!(args, "--" * replace(string(key), '_' => '-'))
        value === true && continue
        push!(args, value)
    end
    return args
end

function run(tagger, switch, kwargs)
    kwargs = Dict{Symbol, Any}(kwargs)
    kwargs[:model_instance] = tagger.model_instance
    kwargs[:sandbox] = tagger.sandbox
    args = Any[]
    parsekwargs!(args, kwargs)
    cmd = tagging_backend_command(tagger)
    Base.run(Cmd(`$cmd $switch $args`; dir=tagger.backend_dir))
end

function train(tagger::Tagger; pretrained_instance=None, kwargs...)
    kwargs = Dict{Symbol, Any}(kwargs)
    kwargs[:pretrained_model_instance] = pretrained_instance
    ret = run(tagger, "train", kwargs)
    @assert isdir(modeldir(tagger))
    return ret
end

predict(tagger::Tagger; kwargs...) = run(tagger, "predict", kwargs)

function finetune(tagger::Tagger; original_instance=nothing, kwargs...)
    kwargs = Dict{Symbol, Any}(kwargs)
    kwargs[:original_model_instance] = original_instance
    ret = run(tagger, "finetune", kwargs)
    @assert isdir(modeldir(tagger))
    return ret
end

embed(tagger::Tagger; kwargs...) = run(tagger, "embed", kwargs)

end # module
