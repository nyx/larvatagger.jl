const LarvaTagger = (function () {

	function timeSlider(element, input, stepmin, stepmax) {
		const cooldown = 0.1; // in seconds
		var mastermin, mastermax, masterpos, heldpos = null, coolingdown = false;
		return {
			element: element,
			input: input,
			stepmin: stepmin,
			stepmax: stepmax,
			timeout: function() {
				if (heldpos === null) {
					coolingdown = false;
				} else {
					this.moveToHeldPosition();
				}
			},
			moveToHeldPosition: function() {
				coolingdown = true;
				slavepos = heldpos;
				heldpos = null;
				// check bounds
				mastermax = stepmax.value;
				if (mastermax < slavepos) {
					slavepos = mastermax;
					element.value = String(slavepos);
				} else {
					mastermin = stepmin.value;
					if (slavepos < mastermin) {
						slavepos = mastermin;
						element.value = String(slavepos);
					}
				}
				// update
				masterpos = input.value;
				if (slavepos != masterpos) {
					input.notify(slavepos);
				}
				// start cooldown
				window.setTimeout(this.timeout, cooldown);
			},
			moveto: function(value) {
				heldpos = parseInt(value);
				if (!coolingdown) {
					this.moveToHeldPosition();
				}
			}
		};
	}

	function focusOnTimeSlider(view) {
		if (view === undefined)
			view = document.getElementById("trackviewer");
		//view.querySelector("input[type=range]").focus()
	}

	function discardLarvaEdits(checkbox, observer, label) {
		if (checkbox.checked) {
			checkbox.checked = false;
		} else {
			var answer = confirm(`Discard changes for larva ${label}?`);
			if (answer) {
				observer.notify(false);
			} else {
				checkbox.checked = true;
			}
		}
	}

	function includeLarva(jsstate, jlstate) {
		if (jsstate.checked) {
			const button = document.getElementById('saveas');
			button.disabled = false;
		} else {
			const includeallcheckbox = document.getElementById('includeall');
			includeallcheckbox.checked = false;
		}
		jlstate.notify(jsstate.checked);
	}

	function includeAllLarvae(jsstate, jlstate, button) {
		let checkboxes = document.getElementsByClassName('included');
		for (let i = 0; i < checkboxes.length; i++) {
			checkboxes[i].checked = jsstate.checked;
		}
		if (button === null) {
			button = document.getElementById('saveas');
		}
		button.disabled = !jsstate.checked;
		jlstate.notify(jsstate.checked);
	}

	function insertNewTag(table, html) {
		const frag = document.createRange().createContextualFragment(html);
		const newtr = frag.firstChild.firstChild.firstChild;
		let tbody = table.getElementsByTagName('tbody')[0];
		if (tbody === undefined) {
			tbody = document.createElement('tbody');
			table.appendChild(tbody);
		}
		tbody.appendChild(newtr);
		table.scrollTop = table.scrollHeight;
	}

	var jlTagSelection;
	function setTagSelector(obs) {
		jlTagSelection = obs;
	}
	function toggleTagAtPointer(event) {
    if (event !== undefined) {
      var that = event.target,
          scroll = that.parentElement.scrollTop;
      event.preventDefault();
      that.selected = !that.selected;
      setTimeout(function(){that.parentElement.scrollTop = scroll;}, 0);
      jlTagSelection.notify(that.value);
    }
		return false;
	}

	function setOutputFilename(obs) {
		var defaultfilepath = obs.value;
		if (defaultfilepath === null) {
			defaultfilepath = "{yyyymmdd_HHMMSS}.label";
		}
		let filepath = prompt("File path: ", defaultfilepath);
		obs.notify(filepath);
	}

	function updateSelectOptions(selectElement, selectOptions, jlObserver) {
		while (selectElement.options.length > 0) {
			selectElement.remove(0);
		}
		for (let i = 0; i < selectOptions.length; i++) {
			const option = document.createElement('option');
			option.value = selectOptions[i];
			option.text = selectOptions[i];
			if (jlObserver !== undefined) {
				option.ondblclick = () => {
					jlObserver.notify(selectOptions[i]);
				};
			}
			selectElement.append(option);
		}
	}

	function pickLocalFiles(jlObserver) {
		let input = document.createElement('input');
		input.type = 'file';
		input.multiple = true;
		input.onchange = () => {
			let files = Array.from(input.files);
			if (files.length > 0) {
				console.log(files);
				jlObserver.notify(files);
			}
		};
		input.click();
	}

	function uncheckAllCheckboxes(elem) {
		let checkboxes = elem.getElementsByTagName('input');
		for (let i = 0; i < checkboxes.length; i++) {
			if (checkboxes[i].type == 'checkbox')
				checkboxes[i].checked = false;
		}
	}

	function confirmDialog(e){
		e.preventDefault();
		e.returnValue = '';
	}

	function reloadPage() {
		window.removeEventListener('beforeunload', confirmDialog);
		location.reload(true);
	}

	return {
		timeSlider,
		focusOnTimeSlider,
		discardLarvaEdits,
		includeLarva,
		includeAllLarvae,
		uncheckAllCheckboxes,
		insertNewTag,
		setTagSelector,
		toggleTagAtPointer,
		setOutputFilename,
		updateSelectOptions,
		pickLocalFiles,
		confirmDialog,
		reloadPage
	};
})();
