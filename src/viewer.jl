
struct ViewerView
    assayplot::AssayViewer
    trackplot::Union{Nothing, TrackViewer}
end

function Bonito.jsrender(session::Session, vv::ViewerView)
    delayed_controller = vv.assayplot.plot.controller

    on(session.on_close) do closed
        if closed
            pause(delayed_controller.player)
        end
    end

    assaydom = r(session, vv.assayplot)
    trackdom = nothing

    if !isnothing(vv.trackplot)
        controller = vv.trackplot.plot.controller
        trackdom = r(session, vv.trackplot)

        on(session, larvaevents(controller).activated) do _
            vv.assayplot.visible[] = false
            evaljs(session,
                js"""
                const assayviewer = $(assaydom);
                const trackviewer = $(trackdom);
                assayviewer.style.display = "none";
                trackviewer.style.display = "block";
                """)
        end
        on(session, larvaevents(controller).deactivated) do _
            vv.assayplot.visible[] = true
            evaljs(session,
                js"""
                const assayviewer = $(assaydom);
                const trackviewer = $(trackdom);
                trackviewer.style.display = "none";
                assayviewer.style.display = "block";
                """)
        end

        r(session,
          DOM.div(Bonito.TailwindCSS, assaydom, trackdom; class="flex flex-row"))

    else
        r(session, DOM.div(Bonito.TailwindCSS, assaydom))
    end
end

function larvaviewer(path::String; allow_multiple_tags::Union{Nothing, Bool}=false,
    title="LarvaTagger", kwargs...)

    App(title=title) do session::Session

        controller = ControllerHub()

        tryopenfile(controller, path)

        viewer = larvaviewer(controller; multipletags=allow_multiple_tags, kwargs...)

        r(session, LarvaTaggerJS)

        dom = Bonito.jsrender(session, viewer)

        r(session, LarvaTaggerCSS)

        return dom
    end
end

function larvaviewer(controller;
        editabletags::Bool=false,
        multipletags::Union{Nothing, Bool}=false,
        viewfactor::Real=1,
    )
    model = LarvaModel[]
    times = PlanarLarvae.Time[0.0, 1.0]
    tag_lut = Observable(TagLUT())
    if haskey(controller, :larva)
        model = getlarvae(controller)
        times = gettimes(controller)
        tag_lut = gettags(controller)
    end

    controller[:player] = player = timecontroller(times)
    controller[:larva] = larva = LarvaController(controller, model, tag_lut, player)

    n_simultaneous_larvae = simultaneouslarvae(model)
    if 100 < n_simultaneous_larvae
        @info "Refresh rate throttled to 1Hz" n_simultaneous_larvae
        delayed_controller = slave(larva, Cooldown(1.0))
    elseif 50 < n_simultaneous_larvae
        @info "Refresh rate throttled to 2Hz" n_simultaneous_larvae
        delayed_controller = slave(larva, Cooldown(0.5))
    else
        delayed_controller = larva
    end

    on(getactivelarva(controller)) do id
        @info isnothing(id) ? "No active larva" : "Activating larva #$(id)"
    end

    kwargs = Dict{Symbol, Any}()
    if viewfactor != 1
        width, height = FIGSIZE
        width = round(Int, width * viewfactor)
        height = round(Int, height * viewfactor)
        kwargs[:size] = (width, height)
    end

    viewer = ViewerView(assayviewer(delayed_controller; kwargs...),
                        trackviewer(controller;
                                    editabletags=editabletags,
                                    multipletags=multipletags,
                                    kwargs...))

    settimestep!(controller, 1)

    return viewer
end
