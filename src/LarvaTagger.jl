module LarvaTagger

using PlanarLarvae, PlanarLarvae.Datasets, PlanarLarvae.Formats
using ObservationPolicies
using TidyObservables
using NyxWidgets, NyxWidgets.Players, NyxWidgets.FileBrowsers, NyxWidgets.FilePickers
import NyxWidgets.Base: lowerdom, dom_id, dom_selector

using Logging
using Bonito, WGLMakie
using Bonito: evaljs, onjs
using Makie
using Format
using Colors
using StaticArrays
using Statistics
using Observables
using Meshes
import Dates
using OrderedCollections
using Random
using LinearAlgebra
using NearestNeighbors

export larvaviewer, larvaeditor

include("misc.jl")
include("models.jl")
include("edits.jl")
include("plots.jl")
include("players.jl")
include("controllers.jl")
include("Taggers.jl")
using .Taggers
include("files.jl")
include("backends.jl")
include("cli_base.jl")

include("wgl.jl")
include("viewer.jl")
include("editor.jl")

end
