
function import′(infile, outfile;
        overrides=nothing,
        runid=nothing,
        defaultlabel=nothing,
        decode=false,
        copylabels=false,
        segment=nothing,
        kwargs...)
    file = load(infile; kwargs...)
    #
    if !isnothing(overrides)
        # note: `getmetadata` acts like `get`, NOT `get!`
        metadata = getmetadata(file)
        metadata[:overrides] = overrides′= Dict{String, Any}()
        for override in split(overrides, ',')
            key, value = split(override, ':'; limit=2)
            try
                value = parse(Int, value)
            catch
                try
                    value = parse(Float64, value)
                catch
                end
            end
            overrides′[key] = value
        end
        # put `metadata` back into `file`,
        # in case the former was initialized by `getmetadata`
        file.run.attributes[:metadata] = metadata
    end
    #
    run = Run(isnothing(runid) ? file.run.id : runid,
              file.run.attributes,
              isa(file, Formats.JSONLabels) ? file.run.tracks : OrderedDict{Datasets.TrackID, Track}())
    if !haskey(run.attributes, :labels)
        run.attributes[:labels] = String[]
    end
    if isa(file, Formats.JSONLabels)
        for dep in getdependencies(file)
            Datasets.pushdependency!(run, dep)
        end
    else
        Datasets.pushdependency!(run, infile)
    end
    if isnothing(defaultlabel)
        if copylabels
            isa(file, Formats.Trxmat) || @warn "Option --copy-labels is designed for trx.mat files only"
            @assert !isa(file, Formats.JSONLabels)
            wrapper = Formats.JSONLabels("")
            wrapper.run = run
            wrapper.timeseries = gettimeseries(file)
            for (trackid, trackdata) in wrapper.timeseries
                track = Track(trackid, times(trackdata),
                              :labels=>[convert(Vector, state[:tags]) for (_, state) in trackdata])
                run[trackid] = track
            end
            run.attributes[:labels] = getlabels(file)
        end
    else
        if defaultlabel isa SubString
            defaultlabel = string(defaultlabel)
        end
        appendlabel!(run, "edited"; ignore=[defaultlabel], ifempty=false)
        setdefaultlabel!(run, defaultlabel; filepath=infile)
    end
    #
    if !decode
        encodelabels!(run)
    end
    if !isnothing(segment)
        if file isa Formats.Trxmat && !copylabels
            @warn "Cropping empty tracks; didn't you forget --copy-labels?"
        end
        if segment isa String || segment isa SubString
            segment = map(split(segment, ',')) do t
                parse(Float64, t)
            end
        end
        t0, t1 = segment
        run = Datasets.segment(run, t0, t1)
    end
    if isnothing(outfile)
        Datasets.write_json(stdout, run)
    else
        if '/' in outfile || '\\' in outfile
            @error "Output file name contains slashes or backslashes (should not be a path)" outfile
            exit()
        end
        outfile = joinpath(dirname(infile), outfile)
        Datasets.to_json_file(outfile, run)
        Taggers.check_permissions(outfile)
    end
end

function merge′(input_path, input_file, edited, output_file=nothing; decode=false)
    if '/' in input_file || '\\' in input_file
        @error "Second input file name contains slashes or backslashes (should not be a path)" input_file
        return
    end
    second_input = joinpath(dirname(input_path), input_file)
    if !isfile(second_input)
        @error "File not found" second_input
        return
    end
    #
    first_input = load(input_path)
    if !(first_input isa Formats.JSONLabels)
        @error "Not a JSON label file" first_input
        return
    end
    second_input = load(second_input)
    if !(second_input isa Formats.JSONLabels)
        @error "Not a JSON label file" second_input
        return
    end
    #
    run = first_input.run
    run2 = second_input.run
    if !shareddependencies(run, run2; extend=true)
        return
    end
    #
    attributes = run.attributes
    attributes2 = run2.attributes
    if !(haskey(attributes, :secondarylabels) && edited ∈ attributes[:secondarylabels])
        @debug "Cannot find manual editions" first_input=basename(first_input.source) edited_label=edited expected_among=get(attributes, :secondarylabels, String[])
    end
    if haskey(attributes2, :secondarylabels) && edited ∈ attributes2[:secondarylabels]
        mergelabels!(run, run2) do labels
            labels isa Vector && edited ∈ labels
        end
    else
        @warn "Cannot find manual editions" second_input=basename(second_input.source) edited_label=edited expected_among=get(attributes2, :secondarylabels, String[])
        mergelabels!(run, run2)
    end
    #
    output = run
    # in the case the first input file was automatically generated, remove the metadata
    # used elsewhere to distinguish between automatically- and manually-assigned labels
    metadata = output.attributes[:metadata]
    if haskey(metadata, :software)
        software = metadata[:software]
        tagger = Datasets.coerce(eltype(keys(software)), :tagger)
        if haskey(software, tagger)
            delete!(software, tagger)
            if isempty(software)
                delete!(metadata, :software)
            end
        end
    end
    #
    if !decode
        encodelabels!(output)
    end
    #
    if isnothing(output_file)
        Datasets.write_json(stdout, output)
    else
        if '/' in output_file || '\\' in output_file
            @error "Output file name contains slashes or backslashes (should not be a path)" outfile
            return
        end
        output_file = joinpath(dirname(input_path), output_file)
        Datasets.to_json_file(output_file, output)
        Taggers.check_permissions(output_file)
    end
end

