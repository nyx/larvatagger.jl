
struct EditorView
    larvaviewer::ViewerView
    larvafilter::LarvaFilter
    tagfilter::TagFilter
    metadataeditor::MetadataEditor
    filemenu::FileMenu
    backendmenu::BackendMenu
    loadanimation::LoadAnimation
    twooptiondialog::TwoOptionDialog
end

function Bonito.jsrender(session::Session, ev::EditorView)
    r(session, LarvaTaggerJS)
    r(session, LoadAwesomeCSS)
    r(session, Bonito.TailwindCSS)
    evaljs(session, js"window.addEventListener('beforeunload', LarvaTagger.confirmDialog)")
    dom = r(session,
      DOM.div(ev.larvaviewer,
              cp(ControlPanel(ev.tagfilter; id="tag-panel", title="Tags"),
                 ControlPanel(ev.larvafilter; id="larva-panel", title="Tracks"),
                 ControlPanel(ev.filemenu, ev.metadataeditor, ev.backendmenu;
                              id="file-panel", title="Files", active=true)),
              ev.loadanimation,
              ev.twooptiondialog;
              class="flex flex-row"))
    r(session, LarvaTaggerCSS)
    return dom
end

projectdir = dirname(Base.active_project())

function larvaeditor(path=nothing;
        allow_multiple_tags::Union{Nothing, Bool}=nothing,
        backend_directory::AbstractString=projectdir,
        manualtag::Union{Nothing, String, Symbol}="edited",
        title="LarvaTagger",
        root_directory=nothing,
        enable_uploads=false,
        enable_downloads=false,
        prepare_download=nothing,
        enable_new_directories=false,
        enable_delete=false,
        kwargs...)

    # to (re-)load a file, the app is reloaded with the filepath as sole information
    # from previous session
    T = Ref{Union{Nothing, String}}
    input = path isa T ? path : T(path)

    App(title=title) do session::Session

        controller = ControllerHub()
        # used for method dispatch (here `Session` is taken to represent Bonito)
        controller[:frontend] = Session

        controller[:manualtag] = manualtag

        if !isnothing(root_directory)
            controller[:workingdirectory] = workingdir(controller, root_directory)
        end

        tryopenfile(controller, input)

        editor = EditorView(larvaviewer(controller;
                                        editabletags=true,
                                        multipletags=allow_multiple_tags,
                                        kwargs...),
                            larvafilter(controller),
                            tagfilter(controller),
                            metadataeditor(controller),
                            filemenu(controller; upload_button=enable_uploads,
                                     download_button=enable_downloads,
                                     prepare_download=prepare_download,
                                     create_directory_button=enable_new_directories,
                                     delete_button=enable_delete),
                            backendmenu(controller, backend_directory),
                            loadanimation(controller),
                            twooptiondialog(controller))

        dom = Bonito.jsrender(session, editor)

        turn_load_animation_off(controller)

        return dom

    end
end
