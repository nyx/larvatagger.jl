# logging

const Stub = LogLevel(500)
const SecurityAlert = LogLevel(2500)

function Base.print(io::IO, level::LogLevel)
    if     level == Logging.BelowMinLevel   print(io, "BelowMinLevel")
    elseif level == Logging.Debug           print(io, "Debug")
    elseif level == Logging.Info            print(io, "Info")
    elseif level == Logging.Warn            print(io, "Warn")
    elseif level == Logging.Error           print(io, "Error")
    elseif level == Logging.AboveMaxLevel   print(io, "AboveMaxLevel")
    elseif level == Stub                    print(io, "Stub")
    elseif level == SecurityAlert           print(io, "SecurityAlert")
    else                                    print(io, "LogLevel($(level.level))")
    end
end

# to be implemented in e.g. wgl.jl

globalrefresh(_, _) = nothing

twooptiondialog(_, _, _, _) = nothing

#getmanualtag(_) = nothing
