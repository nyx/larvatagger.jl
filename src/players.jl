
const O{T} = Union{T, Observable{T}}
using PlanarLarvae: Time
asobservable(arg) = Observable(arg)
asobservable(arg::Observable) = arg

struct TimeController <: AbstractPlayer
    player::AbstractPlayer
    stepmin::AbstractObservable{Int64}
    stepmax::AbstractObservable{Int64}
    boundreached::AbstractObservable{Bool}
end

function TimeController(player::AbstractPlayer)
    stepmin = Observable(min(player))
    stepmax = Observable(max(player))
    TimeController(player, stepmin, stepmax, Observable(false))
end

Players.timestep(controller::TimeController) = timestep(controller.player)
Players.isplaying(controller::TimeController) = isplaying(controller.player)
Base.min(controller::TimeController) = controller.stepmin[]
Base.max(controller::TimeController) = controller.stepmax[]
Players.timestamps(controller::TimeController) = timestamps(controller.player)
Players.speed(controller::TimeController) = Players.speed(controller.player)
Players.repeat(controller::TimeController) = Players.repeat(controller.player)
Players.Model.task(controller::TimeController) = Players.Model.task(controller.player)
function Players.Model.task!(controller::TimeController, task)
    Players.Model.task!(controller.player, task)
end

function timecontroller(times::Vector{Float64}; speed=1.0)
    player = Player(times; speed=speed, loop=nothing)
    ctrl = TimeController(player)
    Players.Model.bind(ctrl, Players.Model.basicloop)
    return ctrl
end

function slave(master::TimeController, policy::ObservationPolicy=IndependentObservables)
    slave_timestep = newobservable(policy, timestep(master))
    slave_player = Player(timestamps(master),
                          slave_timestep,
                          Players.speed(master),
                          isplaying(master),
                          Players.repeat(master),
                          player.task)
    TimeController(slave_player, master.stepmin, master.stepmax, master.boundreached)
end

isslave(controller::TimeController) = ObservationPolicies.haspolicy(timestep(controller))

# function stop!(controller::TimeController)
#     pause(controller)
#     if isslave(controller)
#         policy = timestep(controller).policy
#         # duck typing
#         try
#             ObservationPolicies.stop!(policy)
#         catch
#             @debug "`stop!` not implemented for slave controller $(typeof(policy))"
#         end
#     end
# end

# aliases
gettimes(c::AbstractPlayer) = timestamps(c)[]
gettimestep(c::AbstractPlayer) = timestep(c)
settimestep!(c::AbstractPlayer, t) = (timestep(c)[] = t)
gettimes(c) = gettimes(getplayer(c))
gettimestep(c) = gettimestep(getplayer(c))
settimestep!(c, t) = settimestep!(getplayer(c), t)

function settimebounds!(c::TimeController, larva)
    c.stepmin[] = larva.alignedsteps[1]
    c.stepmax[] = larva.alignedsteps[end]
    @debug "Bounding time" stepmin=c.stepmin[] stepmax=c.stepmax[]
end
function unsettimebounds!(c::TimeController)
    c.stepmin[] = 1
    c.stepmax[] = length(timestamps(c)[])
    @debug "Unbounding time" stepmin=c.stepmin[] stepmax=c.stepmax[]
end
