
struct Backends
    controller
    location::String
    backends::Vector{String}
    active_backend::Observable{Union{Nothing, String}}
    model_instances::Observable{Vector{String}}
    model_instance::Observable{Union{Nothing, String}}
end

function gettagger(model::Backends)
    isnothing(model.model_instance[]) && throw("no model selected")
    backend = model.active_backend[]
    backend_dir = joinpath(model.location, backend)
    instance = model.model_instance[]
    return Tagger(backend_dir, instance)
end

function Backends(controller, location::String)
    isdir(location) || throw("no backends available")
    backends = [dir for dir in readdir(location) if isbackend(joinpath(location, dir))]
    if isempty(backends) && isbackend(location)
        backends = [basename(location)]
        location = dirname(location)
        @warn "Option --backends should point at a backend's parent directory, not the root directory" correct_location=location
    end
    active_backend = Observable{Union{Nothing, String}}(isempty(backends) ? nothing : backends[1])
    model_instances = map(active_backend) do backend
        if isnothing(backend)
            String[]
        else
            models_dir = joinpath(location, active_backend[], "models")
            [dir for dir in readdir(models_dir) if isdir(joinpath(models_dir, dir))]
        end
    end
    model_instance = Observable{Union{Nothing, String}}(nothing)
    on(model_instances) do models
        model_instance[] = isempty(models) ? nothing : models[1]
    end
    on(model_instance) do instance
        @info "Backend selected" backend=active_backend[] instance
    end
    return Backends(controller, location, backends, active_backend, model_instances, model_instance)
end
Backends(controller, location) = Backends(controller, string(location))

function getbackends(controller, location=nothing)
    controller = gethub(controller)
    try
        return controller[:backends]
    catch
        backends = Backends(controller, location)
        Observables.notify(backends.active_backend)
        controller[:backends] = backends
        return backends
    end
end

function Taggers.push(model::Backends, file::String; clean=true, metadata=true)
    tagger = gettagger(model)
    clean ? resetdata(tagger) : resetdata(tagger, "raw")
    dest_file = Taggers.push(tagger, file)
    if metadata
        # save the metadata to file, so that the backend can reproduce them in the output
        # file for predicted labels
        dest_file = joinpath(dirname(dest_file), "metadata")
        metadata = Observables.to_value(getmetadatatable(model.controller))
        PlanarLarvae.Datasets.to_json_file(dest_file, asdict(metadata))
    end
end

function Taggers.pull(model::Backends, destdir::String)
    isnothing(model.model_instance[]) && throw("no model selected")
    backend_dir = joinpath(model.location, model.active_backend[])
    tagger = Tagger(backend_dir, model.model_instance[])
    return Taggers.pull(tagger, destdir)
end

function Taggers.predict(model::Backends)
    isnothing(model.model_instance[]) && throw("no model selected")
    backend_dir = joinpath(model.location, model.active_backend[])
    model_instance = model.model_instance[]
    isnothing(model_instance) && throw("no model instance selected")
    turn_load_animation_on(model.controller)
    try
        # TODO: make the skip_make_dataset option discoverable in the backend
        predict(Tagger(backend_dir, model_instance); skip_make_dataset=true)
    catch
        turn_load_animation_off(model.controller)
        rethrow()
    end
end

function Taggers.predict(model::Backends, file::String)
    Taggers.push(model, file)
    predict(model)
    labelfile = Taggers.pull(model, dirname(file))
    @assert length(labelfile) == 1
    tryopenfile(model.controller, labelfile[1]; reload=true)
end
