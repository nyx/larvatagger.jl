module GUI

using DocOpt
using LarvaTagger
using Bonito: Bonito, Server

export main

usage = """LarvaTagger.jl - launch the server-based GUI.

Usage:
  larvatagger-gui.jl [<file-path>] [--backends=<path>] [--port=<number>] [--server-url=<url>] [--quiet] [--viewer] [--browser] [--view-factor=<real>] [--manual-label=<label>]
  larvatagger-gui.jl -h | --help

Options:
  -h --help               Show this screen.
  -q --quiet              Do not show instructions.
  --backends=<path>       Path to backend repository.
  --port=<number>         Port number the server listens to.
  --server-url=<url>      Server address, for remote access.
  --viewer                Disable editing capabilities.
  --browser               Automatically open a browser tab at the served location.
  --view-factor=<real>    Scaling factor for the larva views; default is 2 on macOS, 1 elsewhere.
  --manual-label=<label>  Secondary label for manually labelled data [default: edited].

The optional positional argument <file-path> can also be the data root directory.
Backends defined in LarvaTagger project root directory are automatically found. Other
backend locations can be specified with the --backends argument.

By default, if --server-url is specified, the port is appended to the ip address or domain
name. To prevent this behavior, include the desired port number in the url.
"""

function main(args=ARGS; exit_on_error=false)
    if isempty(args)
        print(usage)
        exit()
    end

    parsed_args = try
        docopt(usage, args isa String ? split(args) : args;
               help=false, exit_on_error=exit_on_error)
    catch # for exit_on_error==false only
        print("Parsing error -- ") # help message follows
        true # docopt returns true if no input arguments are passed
    end

    if parsed_args === true || parsed_args["--help"]
        print(usage)
        exit()
    end

    verbose = !parsed_args["--quiet"]
    infile = parsed_args["<file-path>"]
    if isempty(infile)
        infile = nothing
    elseif !isfile(infile)
        if isdir(infile)
            dataroot = infile
            infile = nothing
            cd(dataroot)
        else
            @error "File not found; did you specify a file path?" infile
            exit()
        end
    end

    kwargs = Dict{Symbol, Any}()
    viewfactor = parsed_args["--view-factor"]
    if !isnothing(viewfactor)
        kwargs[:viewfactor] = parse(Float64, viewfactor)
    # elseif Sys.isapple()
    #     kwargs[:viewfactor] = 2
    end

    if parsed_args["--viewer"]
        app = larvaviewer(infile; kwargs...)
    else
        backends = parsed_args["--backends"]
        if !isnothing(backends)
            kwargs[:backend_directory] = backends
        end
        kwargs[:manualtag] = string(parsed_args["--manual-label"])
        app = larvaeditor(infile; kwargs...)
    end
    #
    port = parsed_args["--port"]
    port = isnothing(port) ? 9284 : parse(Int, port)
    proxy_url = parsed_args["--server-url"]
    if isnothing(proxy_url)
        proxy_url = ""
    elseif !startswith(proxy_url, "http")
        proxy_url = "http://$(proxy_url)"
    end
    server = Server(app, "0.0.0.0", port; proxy_url=proxy_url)
    port = server.port
    if !isempty(proxy_url)
        protocol, remainder = split(proxy_url, "://")
        if !(':' in remainder)
            server.proxy_url = proxy_url = if '/' in remainder
                server_name, path = split(remainder, '/'; limit=2)
                "$(protocol)://$(server_name):$(port)/$(path)"
            else
                "$(protocol)://$(remainder):$(port)"
            end
        end
    end
    if parsed_args["--browser"]
        Bonito.HTTPServer.openurl("http://127.0.0.1:$(port)")
    end
    if verbose
        @info "The server is ready at $(Bonito.HTTPServer.online_url(server, ""))"
    end
    if verbose
        @info "Press Ctrl+D to stop the server (or Ctrl+C if in PowerShell)"
    end
    return server

end

end
