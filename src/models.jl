const AbstractObservable{T} = Observables.AbstractObservable{T}

PlanarLarvae.vertices′(T::Type{<:Makie.Point{Dim, <:AbstractFloat}}, points::Vector{<:Meshes.Point{Dim, <:AbstractFloat}}) where {Dim} = [T(coordinates(p)...) for p in points]

const TimeStep = Int
const PathOrOutline = Vector{Makie.Point2f}
const Name = Union{Symbol, String}
const Color = Union{Symbol, String, <:AbstractRGBA}
const OptionalColor = Union{Nothing, Symbol, String, <:AbstractRGBA}

#

html_color(color) = "#$(hex(to_color(color), :rrggbb))"

abstract type AbstractTag end

struct TagModel <: AbstractTag
    name::Name
    color::OptionalColor
    active::Union{Nothing, Bool}
    secondary::Bool
    frozen::Bool
end

function TagModel(name::Name, secondary::Bool=false; frozen::Bool=false)
    TagModel(name, nothing, nothing, secondary, frozen)
end
function TagModel(name::Name,
        color::OptionalColor,
        active::Union{Nothing, Bool},
        secondary::Bool=false,
    )
    TagModel(name, color, active, secondary, false)
end

isfrozen(tag::TagModel) = tag.frozen
isfrozen(::Nothing) = false

struct ObservableTag <: AbstractTag
    name::AbstractObservable{String}
    color::AbstractObservable{String}
    active::AbstractObservable{Bool}
    original::Union{Nothing, TagModel}
end

function ObservableTag(
        name::Name,
        color::Color,
        active::Bool=true,
        original::Union{Nothing, TagModel}=nothing,
    )
    ObservableTag(Observable(string(name)),
                  Observable(html_color(color)),
                  Observable(active),
                  original)
end

function ObservableTag(model::TagModel;
        color::OptionalColor=nothing,
        active::Union{Nothing, Bool}=nothing,
    )
    observable_color = @something color model.color
    observable_active = @something active (@something model.active true)
    ObservableTag(model.name, observable_color, observable_active, model)
end

ObservableTag() = ObservableTag("", "#000000")

isfrozen(t::ObservableTag) = isfrozen(t.original)

isnative(t::ObservableTag) = !isnothing(t.original)

const TagLUT = Vector{ObservableTag}
const UserTags = Vector{ObservableTag} # ordered set

function settag!(tags::UserTags, tag::ObservableTag; single::Bool=false)
    if single
        empty!(tags)
        push!(tags, tag)
    else
        filter!(tag′ -> tag′ !== tag, tags)
        pushfirst!(tags, tag)
    end
end

function deletetag!(tags::UserTags, tag::ObservableTag)
    filter!(tag′ -> tag′ !== tag, tags)
end

deletetags!(tags::UserTags) = empty!(tags)

function lookup(lut::TagLUT, tagname::String, currentonly::Bool=false)
    if !currentonly
        for tag in lut
            if !isnothing(tag.original) && tag.original.name === Symbol(tagname)
                return tag
            end
        end
    end
    for tag in lut
        if tag.name[] == tagname
            return tag
        end
    end
end

function lookup(lut, tagname::String, currentonly::Bool=false)
    if !currentonly
        for tag in lut
            tag′ = tag.model
            if !isnothing(tag′.original) && tag′.original.name === Symbol(tagname)
                return tag
            end
        end
    end
    for tag in lut
        if tag.model.name[] == tagname
            return tag
        end
    end
end

function getusertags(larva, timestep; lut=nothing)
    usertags = larva.usertags[]
    tags = UserTags()
    if haskey(usertags, timestep)
        tags = usertags[timestep]
    else
        firststep = larva.alignedsteps[1]
        firststep <= timestep || return tags
        relstep = timestep - firststep + 1
        relstep ∈ larva.missingsteps && return tags
        relstep -= count(larva.missingsteps .< relstep)
        if relstep > length(larva.fullstates)
            ts = [ s[1] for s in larva.fullstates ]
            @error "" larva.missingsteps ts
        end
        _, state = larva.fullstates[relstep]
        hasproperty(state, :tags) || return tags
        tagnames = convert(Vector{String}, state.tags)
        lut = Observables.to_value(lut)
        for tagname in tagnames
            tag = lookup(lut, tagname)
            if isnothing(tag)
                @error "Tag \"$tagname\" not found in LUT" lut
            else
                push!(tags, tag)
            end
        end
    end
    return tags
end

function getusertags!(larva, timestep; lut=nothing)
    usertags = larva.usertags[]
    tags = UserTags()
    try
        tags = usertags[timestep]
    catch
        firststep = larva.alignedsteps[1]
        relstep = timestep - firststep + 1
        relstep ∈ larva.missingsteps && return tags # do not store in usertags
        relstep -= count(larva.missingsteps .< relstep)
        _, state = larva.fullstates[relstep]
        if hasproperty(state, :tags)
            tagnames = convert(Vector{String}, state.tags)
            lut = Observables.to_value(lut)
            for tagname in tagnames
                tag = lookup(lut, tagname)
                if isnothing(tag)
                    @error "Tag \"$tagname\" not found in LUT" lut
                else
                    push!(tags, tag)
                end
            end
        end
        usertags[timestep] = tags
    end
    return tags
end

function gettag(tag_lut, larva, alignedstep, default_color)
    tags = getusertags(larva, alignedstep; lut=tag_lut)
    #tag_lut = Observables.to_value(tag_lut)
    #for tag in tag_lut # the LUT defines the order of the tags
    for tag in tags
        if tag.active[]# && tag in tags
            return (tag.name[], tag.color[])
        end
    end
    return (nothing, default_color)
end

function gettag(tags::UserTags, tag_lut)
    # tag_lut = Observables.to_value(tag_lut)
    # for tag in tag_lut # the LUT defines the order of the tags
    for tag in tags
        if tag.active[]# && tag in tags
            return tag
        end
    end
end

function freezetags(tags::Dict{TimeStep, UserTags}, from, to)
    frozen_tagnames = Dict{TimeStep, Vector{String}}()
    for step in from:to
        try
            tags′ = tags[step]
            frozen_tagnames[step] = [tag.name[] for tag in tags′]
        catch
        end
    end
    return frozen_tagnames
end

function TidyObservables.newcontroller(tag::ObservableTag, taglut::AbstractObservable)
    ctrl = newcontroller(tag, :name, :color)
    on(ctrl, :name) do name, current
        if isempty(name)
            Invalidate(; revert=!isempty(current))
        elseif isfrozen(tag)
            Invalidate(; revert=name != current)
        elseif exists(name, tag, taglut)
            Invalidate()
        else
            Validate(name)
        end
    end
    on(html_color, ctrl, :color, Val(1))
    return ctrl
end

struct LarvaModel
    id::LarvaID
    alignedsteps::Vector{TimeStep}
    missingsteps::Vector{Int}
    path::PathOrOutline
    fullstates::PlanarLarvae.TimeSeries{<:NamedTuple}
    usertags::AbstractObservable{Dict{TimeStep, UserTags}}
end

function LarvaModel(id::LarvaID, timeseries::PlanarLarvae.TimeSeries{<:NamedTuple}, times::Vector{PlanarLarvae.Time})
    @assert !isempty(times)
    alignedsteps = map(timeseries) do (t, _)
        findfirst(t′-> abs(t - t′) < TIME_PRECISION, times)
    end
    issorted(alignedsteps) || throw("Time steps are not strictly increasing in track {track.id}")
    steps = @. alignedsteps - alignedsteps[1] + 1
    missingsteps = ones(Bool, steps[end])
    missingsteps[steps] .= false
    missingsteps = findall(missingsteps)
    isempty(missingsteps) || @warn "Time steps are missing" id=convert(Int, id) missingsteps
    path = coordinates.(larvatrack(timeseries))
    usertags = Observable(Dict{TimeStep, UserTags}())
    LarvaModel(id, alignedsteps, missingsteps, path, timeseries, usertags)
end

function LarvaModel(track::Track, times::Vector{PlanarLarvae.Time})
    @assert !isempty(times)
    alignedsteps = map(track.timestamps) do t
        findfirst(t′-> abs(t - t′) < TIME_PRECISION, times)
    end
    issorted(alignedsteps) || throw("Time steps are not strictly increasing in track {track.id}")
    steps = @. alignedsteps - alignedsteps[1] + 1
    missingsteps = ones(Bool, steps[end])
    missingsteps[steps] .= false
    missingsteps = findall(missingsteps)
    isempty(missingsteps) || @warn "Time steps are missing" id=convert(Int, track.id) missingsteps
    path = coordinates.(larvatrack(track[:spine]))
    usertags = Observable(Dict{TimeStep, UserTags}())
    LarvaModel(track.id,
               alignedsteps,
               missingsteps,
               path,
               astimeseries(track),
               usertags)
end

"""
    outline_or_spine(state)
    outline_or_spine(pointtype, state)

Return outline data if available, spine data otherwise.
"""
function outline_or_spine(state)
    if haskey(state, :outline)
        outline(state)
    else
        spine(state)
    end
end
function outline_or_spine(T, state)
    if haskey(state, :outline)
        outline(T, state)
    else
        spine(T, state)
    end
end

Meshes.boundingbox(larva::LarvaModel) = Meshes.boundingbox([outline_or_spine(state) for (_,state) in larva.fullstates])
Meshes.boundingbox(larvae::Vector{LarvaModel}) = Meshes.boundingbox(map(Meshes.boundingbox, larvae))

function downsampler(; step::Int=20)
    function sample(larva::LarvaModel)
        _, sparsepath = downsample(larva.alignedsteps, larva.path; step=step)
        LarvaModel(larva.id,
                   larva.alignedsteps,
                   larva.missingsteps,
                   sparsepath,
                   larva.fullstates,
                   larva.usertags)
    end
end

function downsample(timestamps::Vector{Int}, path::PathOrOutline; step::Int=20)
    # TODO: optimize `path` wrt space only, and forget about time;
    #       there's no need to align `path` on anything else;
    #       `alignedsteps` and `missingsteps` only need to match `fullstates` and `usertags`
    # important: `downsample` should only apply to the multi-larva view; single-larva view
    #            relies on `path` to match with `alignedsteps` and `missingsteps`;
    #            see for example function `larva_hovered` in plots.jl
    start = mod1(timestamps[1], step)
    (timestamps[start:step:end], path[start:step:end])
end

medianlarvasize(larvae::Dict; n::Int=100) = medianlarvasize(collect(values(larvae)); n=n)

function medianlarvasize(larvae::Vector{LarvaModel}; n::Int=100)::Float32
    isempty(larvae) && return 0.0f0
    larvae = Random.shuffle(larvae)
    sizes = Float64[]
    sizehint!(sizes, n)
    for k in 1:n
        larva = larvae[mod1(k, length(larvae))]
        _, state = larva.fullstates[rand(1:length(larva.fullstates))]
        spine = state[:spine]
        size = sum(norm.(diff(spine.vertices)))
        push!(sizes, size)
    end
    return median(sizes)
end

function simultaneouslarvae(larvae)::Int
    isempty(larvae) && return 0
    laststep = maximum([larva.alignedsteps[end] for larva in values(larvae)])
    n = 0
    for step in 1:20:laststep
        n = max(n, count(larva -> larva.alignedsteps[1] <= step <= larva.alignedsteps[end],
                         values(larvae)))
    end
    return n
end
