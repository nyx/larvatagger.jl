module Toolkit

using DocOpt
using Pkg
using OrderedCollections
using PlanarLarvae, PlanarLarvae.Datasets, PlanarLarvae.Formats, PlanarLarvae.Dataloaders

include("Taggers.jl")
using .Taggers

include("cli_base.jl")

export main

usage = """Larva Tagger.

Usage:
  larvatagger-toolkit.jl import <input-path> [<output-file>] [--id=<id>] [--framerate=<fps>] [--pixelsize=<μm>] [--overrides=<comma-separated-list>] [--default-label=<label>] [--manual-label=<label>] [--decode] [--copy-labels] [--segment=<t0,t1>]
  larvatagger-toolkit.jl train <backend-path> <data-path> <model-instance> [--pretrained-model=<instance>] [--labels=<comma-separated-list>] [--sample-size=<N>] [--balancing-strategy=<strategy>] [--class-weights=<csv>] [--manual-label=<label>] [--layers=<N>] [--iterations=<N>] [--seed=<seed>] [--debug]
  larvatagger-toolkit.jl train <backend-path> <data-path> <model-instance> --fine-tune=<instance> [--balancing-strategy=<strategy>] [--manual-label=<label>] [--iterations=<N>] [--seed=<seed>] [--debug]
  larvatagger-toolkit.jl predict <backend-path> <model-instance> <data-path> [--output=<filename>] [--make-dataset] [--skip-make-dataset] [--data-isolation] [--debug]
  larvatagger-toolkit.jl predict <backend-path> <model-instance> <data-path> --embeddings [--data-isolation] [--debug]
  larvatagger-toolkit.jl merge <input-path> <input-file> [<output-file>] [--manual-label=<label>] [--decode]
  larvatagger-toolkit.jl -V | --version
  larvatagger-toolkit.jl -h | --help

Options:
  -h --help            Show this screen.
  -V --version         Show version.
  --id=<id>            Run or assay ID, e.g. `date_time`.
  --framerate=<fps>    Camera frame rate, in frames per second.
  --pixelsize=<μm>     Camera pixel size, in micrometers.
  --make-dataset       Perform the make_dataset stage prior to proceeding to predict_model.
  --skip-make-dataset  Skip the make_dataset stage (default behavior since TaggingBackends==0.10).
  --data-isolation     Isolate the backend data directories for parallel tagging of multiple data files.
  --sample-size=<N>    Sample only N track segments from the data repository.
  --layers=<N>         (MaggotUBA) Number of layers of the classifier.
  --iterations=<N>     Number of training iterations (integer or comma-separated list of integers).
  --seed=<seed>        Seed for the backend's random number generators.
  --segment=<t0,t1>    Start and end times (included, comma-separated) for cropping and including tracks.
  --debug              Lower the logging level to DEBUG.
  --embeddings         (MaggotUBA) Call the backend to generate embeddings instead of labels.
  --decode             Do not encode the labels into integer indices.
  --copy-labels        Replicate discrete behavior data from the input file.
  --default-label=<label>             Label all untagged data as <label>.
  --manual-label=<label>              Secondary label for manually labelled data [default: edited].
  --labels=<comma-separated-list>     Comma-separated list of behavior tags/labels.
  --class-weights=<csv>               Comma-separated list of floats.
  --pretrained-model=<instance>       Name of the pretrained encoder (from `pretrained_models` registry).
  --balancing-strategy=<strategy>     Any of `auto`, `maggotuba`, `none` [default: auto].
  --fine-tune=<instance>              Load and fine-tune an already trained model.
  --overrides=<comma-separated-list>  Comma-separated list of key:value pairs.
  -o <filename> --output=<filename>   Predicted labels filename.


Commands:

  import    Generate a label file and store metadata.

    This command has several use cases:
    * it allows specifying missing information such as the camera framerate for FIMTrack v2
      table.csv files. A label file is created with a pointer to the original csv file, and
      a metadata section with the provided information.
    * for label files specifically, it can extend the tracks and timesteps on basis of the
      associated data dependencies and assigning a default label to the data points that are
      not already defined in the label file.
    * it can export the discrete behavior information from trx.mat files and save this
      information to label files. This is done with the --copy-labels flag. This argument is
      implicitly true if a default label is defined.
    Note: label files should always come as siblings of their data dependencies (located in
    the same directory).

  merge     Combine the content of label files.

    <input-path> is the path to a manually edited label file whose content is to be augmented
    with manual editions from a second (sibling) label file <input-file>. If no output file
    is specified, the resulting content is dumped onto the standard output. All label files
    must be siblings, i.e. located in the same directory.
    If no editions are found in the second input file, all the defined labels are copied from
    the second to the first file.

  train     Train a tagger.

    <data-path> can be a path to a file or directory.
    --class-weights requires --labels to be defined and the specified comma-separated values
    should match those given by --labels.
    --fine-tune acts like a child switch and leads to separate logic. Class labels and
    weights are inherited from the model instance to further train. Underrepresented classes
    cannot be excluded but the corresponding track points can be unlabelled to make the
    classes just missing.

  predict   Automatically label tracking data.

    <data-path> can be a path to a file or directory.
    <data-path> can also be a .txt file listing data files; one relative path per line.

"""

function main(args=ARGS; exit_on_error=true)
    parsed_args = try
        docopt(usage, args isa String ? split(args) : args;
               help=false, exit_on_error=exit_on_error)
    catch # for exit_on_error==false only
        print("Parsing error -- ") # help message follows
        Dict{String, Any}()
    end

    if isempty(parsed_args) || parsed_args["--help"]
        print(usage)

    elseif parsed_args["--version"]
        println(Pkg.project().version)

    elseif parsed_args["import"]
        infile = parsed_args["<input-path>"]
        if !isfile(infile)
            @error "File not found; did you specify a file path after \"import\"?" infile
            exit()
        end
        outfile = parsed_args["<output-file>"]
        kwargs = Dict{Symbol, Any}()
        framerate = parsed_args["--framerate"]
        if !isnothing(framerate)
            kwargs[:framerate] = parse(Float64, framerate)
        end
        pixelsize = parsed_args["--pixelsize"]
        if !isnothing(pixelsize)
            kwargs[:pixelsize] = parse(Float64, pixelsize)
        end
        kwargs[:overrides] = parsed_args["--overrides"]
        kwargs[:runid] = parsed_args["--id"]
        kwargs[:defaultlabel] = parsed_args["--default-label"]
        kwargs[:decode] = parsed_args["--decode"]
        kwargs[:copylabels] = parsed_args["--copy-labels"]
        kwargs[:segment] = parsed_args["--segment"]
        import′(infile, outfile; kwargs...)

    elseif parsed_args["merge"]
        input_path = parsed_args["<input-path>"]
        if !isfile(input_path)
            @error "File not found; did you specify a file path after \"combine\"?" input_path
            exit()
        end
        input_file = parsed_args["<input-file>"]
        edited = parsed_args["--manual-label"]
        decode = parsed_args["--decode"]
        outfile = parsed_args["<output-file>"]
        merge′(input_path, input_file, edited, outfile; decode=decode)

    elseif parsed_args["train"]
        backend_path = parsed_args["<backend-path>"]
        model_instance = parsed_args["<model-instance>"]
        data_path = parsed_args["<data-path>"]
        #
        tagger = Tagger(backend_path, model_instance)
        Taggers.reset(tagger)
        Taggers.push(tagger, data_path)
        # common arguments
        kwargs = Dict{Symbol, Any}()
        kwargs[:balancing_strategy] = parsed_args["--balancing-strategy"]
        kwargs[:include_all] = parsed_args["--manual-label"]
        # additional arguments whose default value is not `nothing`
        iterations = parsed_args["--iterations"]
        isnothing(iterations) || (kwargs[:iterations] = iterations)
        seed = parsed_args["--seed"]
        isnothing(seed) || (kwargs[:seed] = seed)
        debug = parsed_args["--debug"]
        isnothing(debug) || (kwargs[:debug] = debug)
        #
        finetune_model = parsed_args["--fine-tune"]
        if isnothing(finetune_model) # standard train
            kwargs[:pretrained_instance] = parsed_args["--pretrained-model"]
            kwargs[:sample_size] = parsed_args["--sample-size"]
            kwargs[:labels] = parsed_args["--labels"]
            #
            layers = parsed_args["--layers"]
            isnothing(layers) || (kwargs[:layers] = layers)
            classweights = parsed_args["--class-weights"]
            isnothing(classweights) || (kwargs[:class_weights] = classweights)
            #
            train(tagger; kwargs...)
        else # fine-tuning
            kwargs[:original_instance] = finetune_model
            finetune(tagger; kwargs...)
        end

    elseif parsed_args["predict"]
        backend_path = parsed_args["<backend-path>"]
        model_instance = parsed_args["<model-instance>"]
        data_path = parsed_args["<data-path>"]
        data_isolation = parsed_args["--data-isolation"]
        output_filename = parsed_args["--output"]
        embeddings = parsed_args["--embeddings"]
        #
        datapath = abspath(data_path)
        destination = if isfile(datapath)
            if endswith(datapath, ".txt")
                # assume that the file lists paths to data files to be processed;
                # in this case, the path to the txt file is not relevant
                @info "The listed data file paths are expected to be relative to current directory" cwd=pwd()
                pwd()
            else
                dirname(datapath)
            end
        elseif occursin("*", datapath)
            repository = Dataloaders.Repository(datapath)
            repository.root
        else
            datapath
        end
        #
        tagger = Tagger(backend_path, model_instance)
        if !isnothing(output_filename)
            tagger.output_filenames["predicted.label"] = output_filename
        end
        if data_isolation
            tagger = Taggers.isolate(tagger)
            @info "Tagger isolated" sandbox=tagger.sandbox
        end
        resetdata(tagger)
        Taggers.push(tagger, datapath)
        if embeddings
            embed(tagger; skip_make_dataset=parsed_args["--skip-make-dataset"],
                make_dataset=parsed_args["--make-dataset"], debug=parsed_args["--debug"])
        else
            predict(tagger; skip_make_dataset=parsed_args["--skip-make-dataset"],
                make_dataset=parsed_args["--make-dataset"], debug=parsed_args["--debug"])
        end
        Taggers.pull(tagger, destination)
    end
end

end
