
projectdir = dirname(Base.active_project())
include(joinpath(projectdir, "src", "cli_open.jl"))
include(joinpath(projectdir, "src", "cli_toolkit.jl"))

using .GUI
using .Toolkit

usage = """Larva Tagger.

Usage:
  larvatagger.jl open [<file-path>] [--backends=<path>] [--port=<number>] [--quiet] [--viewer] [--browser] [--view-factor=<real>] [--manual-label=<label>]
  larvatagger.jl import <input-path> [<output-file>] [--id=<id>] [--framerate=<fps>] [--pixelsize=<μm>] [--overrides=<comma-separated-list>] [--default-label=<label>] [--manual-label=<label>] [--decode] [--copy-labels]
  larvatagger.jl train <backend-path> <data-path> <model-instance> [--pretrained-model=<instance>] [--labels=<comma-separated-list>] [--sample-size=<N>] [--balancing-strategy=<strategy>] [--class-weights=<csv>] [--manual-label=<label>] [--layers=<N>] [--iterations=<N>] [--seed=<seed>] [--debug]
  larvatagger.jl train <backend-path> <data-path> <model-instance> --fine-tune=<instance> [--balancing-strategy=<strategy>] [--manual-label=<label>] [--iterations=<N>] [--seed=<seed>] [--debug]
  larvatagger.jl predict <backend-path> <model-instance> <data-path> [--output=<filename>] [--make-dataset] [--skip-make-dataset] [--data-isolation] [--debug]
  larvatagger.jl predict <backend-path> <model-instance> <data-path> --embeddings [--data-isolation] [--debug]
  larvatagger.jl merge <input-path> <input-file> [<output-file>] [--manual-label=<label>] [--decode]
  larvatagger.jl -V | --version
  larvatagger.jl -h | --help

Options:
  -h --help             Show this screen.
  -V --version          Show version.
  -q --quiet            Do not show instructions.
  --id=<id>             Run or assay ID, e.g. `date_time`.
  --framerate=<fps>     Camera frame rate, in frames per second.
  --pixelsize=<μm>      Camera pixel size, in micrometers.
  --backends=<path>     Path to backend repository.
  --port=<number>       Port number the server listens to.
  --viewer              Disable editing capabilities.
  --browser             Automatically open a browser tab at the served location.
  --view-factor=<real>  Scaling factor for the larva views; default is 2 on macOS, 1 elsewhere.
  --make-dataset        Perform the make_dataset stage prior to proceeding to predict_model.
  --skip-make-dataset   Skip the make_dataset stage (default behavior since TaggingBackends==0.10).
  --data-isolation      Isolate the backend data directories for parallel tagging of multiple data files.
  --sample-size=<N>     Sample only N track segments from the data repository.
  --layers=<N>          (MaggotUBA) Number of layers of the classifier.
  --iterations=<N>      (MaggotUBA) Number of training iterations (can be two integers separated by a comma).
  --seed=<seed>         Seed for the backend's random number generators.
  --segment=<t0,t1>     Start and end times (included, comma-separated) for cropping and including tracks.
  --debug               Lower the logging level to DEBUG.
  --embeddings          (MaggotUBA) Call the backend to generate embeddings instead of labels.
  --decode              Do not encode the labels into integer indices.
  --copy-labels         Replicate discrete behavior data from the input file.
  --default-label=<label>             Label all untagged data as <label>.
  --manual-label=<label>              Secondary label for manually labelled data [default: edited].
  --labels=<comma-separated-list>     Comma-separated list of behavior tags/labels.
  --class-weights=<csv>               Comma-separated list of floats.
  --pretrained-model=<instance>       Name of the pretrained encoder (from `pretrained_models` registry).
  --balancing-strategy=<strategy>     Any of `auto`, `maggotuba`, `none` [default: auto].
  --fine-tune=<instance>              Load and fine-tune an already trained model.
  --overrides=<comma-separated-list>  Comma-separated list of key:value pairs.
  -o <filename> --output=<filename>   Predicted labels filename.


Commands:

  open      Launch the server-based GUI.

    The optional positional argument <file-path> can also be the data root directory.
    Backends defined in LarvaTagger project root directory are automatically found. Other
    backend locations can be specified with the --backends argument.

  import    Generate a label file and store metadata.

    This command is particularly useful to specify missing information such as the camera
    framerate for FIMTrack v2 table.csv files. A label file is created with a pointer to the
    original csv file, and a metadata section with the provided information.
    A second usage applies to label files specifically, and consists in extending the tracks
    and timesteps on basis of the associated data dependencies and assigning a default label
    to the data points that are not already defined in the label file.
    Note: label files should always come as siblings of their data dependencies (located in
    the same directory).

  merge     Combine the content of label files.

    <input-path> is the path to a manually edited label file whose content is to be augmented
    with manual editions from a second (sibling) label file <input-file>. If no output file
    is specified, the resulting content is dumped onto the standard output. All label files
    must be siblings, i.e. located in the same directory.
    If no editions are found in the second input file, all the defined labels are copied from
    the second to the first file.

  train     Train a tagger.

    <data-path> can be a path to a file or directory.
    --class-weights requires --labels to be defined and the specified comma-separated values
    should match those given by --labels.

  predict   Automatically label tracking data.

    <data-path> can be a path to a file or directory.
    <data-path> can also be a .txt file listing data files; one relative path per line.

"""

function main(args=ARGS; kwargs...)
    if args isa String
        args = split(args)
    end
    if isempty(args) || "-h" in args || "--help" in args
        print(usage)
    elseif args[1] == "open"
        GUI.main(args[2:end]; kwargs...)
    else
        Toolkit.main(args; kwargs...)
    end
end
